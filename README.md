# RVGL Documentation

## Building the Docs Locally

Install mdbook and mdbook-toc. Then run `mdbook build` or `mdbook serve` in the project folder.

The docs are automatically built with CI after pushing to GitLab.
