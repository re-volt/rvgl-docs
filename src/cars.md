# For Car Makers

<!-- toc -->

Customize user cars through new entries in the car's "parameters.txt" file.

> For compatibility with the original versions of the game, the "parameters.txt"
specification supports a *comment closer*. This is a closing paranthesis following
the semi-colon, or `;)`. Prepending this at each line causes the line to be skipped
by the original versions while it is still read by RVGL. A line can be included
twice, first without the comment closer and then with. Eg.,
> ```
> Rating      4  ; Pro in original version
> ;)Rating    5  ; Super Pro in RVGL
> ```

## Carbox Art

To add a box-art image to your car, use the TCARBOX parameter. This entry works the
same way as the TPAGE parameter. General syntax:
```
TCARBOX    <path to carbox texture>
```
​
It is recommended that the carbox texture use a pure white (RGB 255,255,255) background.

## Custom Car Sound

The keyword SFXENGINE adds a custom engine sound for your car. The way the engine
sound is used by the game depends on the car class (Electric or Glow). The keyword
SFXHONK adds a custom horn sound. The keyword SFXSERVO adds a custom servo
sound (i.e., wheel turning effect).

General syntax:
```
SFXENGINE    <path to wav file>
SFXHONK      <path to wav file>
SFXSERVO     <path to wav file>
```

## Custom Shadow

Each car can have its own shadow map, or it can use one of the preset stock
shadows (useful for default car repaints).

The shadow map is generally a 64x64 texture containing a white silhouette of the car
on a black background. They can be generated from the top view of the car
(preferably from a 3D modeller) and then post-processed in an image editor.

RVGL requires 5 coordinates to position the shadow under the car. These are the
Left, Right, Front and Back edges and Height relative to the model center.
These values are independent of the car CoM and do not need to be updated upon
changing the CoM.

Parameters to be added:

+ `TSHADOW` to specify the shadow texture, similar to TPAGE and TCARBOX
    parameters. This parameter can be ignored if the car is not using a
    custom shadow texture.
+ `SHADOWINDEX` to use one of the default car shadows found in
    "cars\\misc\\shadow.bmp". Set this parameter to -1 to use the generic
    rectangle shadow. This parameter can be ignored if TSHADOW is used.
+ `SHADOWTABLE` followed by 5 offset values for the Left / Right / Front /
    Back / Height of shadow relative to model center. Remove this line or set
    all offsets to zero if you'd like the shadow to be automatically placed
    under the car's body (in this case, the shadow will not cover wheels
    extending out of the body).

General syntax:
```
TSHADOW        <path to shadow texture (or) NONE>
SHADOWINDEX    -1  ; 0 to 27, (or) -1
SHADOWTABLE    -10.2 10.2 30.4 -30.4 -4.2
```

## Stats Display

User-made cars can show the Speed / Acceleration / Weight bars in the car
selection screen.

To use this feature, calculate and fill in the TopEnd and Acc parameter values
using the *Calc Car Stats* option in Dev mode. Weight takes the same value as the
car mass. RVGL also requires the Trans parameter to be properly filled in.

Then, add the following line in the Frontend section of the parameters file.
```
Statistics     TRUE
```

>  TopEnd gives the maximum top-speed that can actually be reached by the car.
Acc gives time, in seconds, the car takes to reach the top speed.

## Hood & Rear Camera

Configure the Hood and Rearview cameras for your car by adding the `CAMATTACHED`
section in your parameters file. Below is a sample:
```
;====================
; Camera details
;====================

CAMATTACHED {  ; Start Camera
HoodOffset     -0.12 -71.81 -60.01  ; Offset from model center
HoodLook       0.02  ; Look angle (-0.25 to 0.25, 0.0 - straight)
RearOffset     -0.12 -71.81 27.24
RearLook       0.03
FixedOffset    true               ; Is offset fixed or moving
FixedLook      true               ; Is look fixed or moving
}              ; End Camera
```

## Camber Angle

Configure the camber angle for each wheel of your car using the `Camber` entry.
This sets the rotation angle (in degrees) of the wheel along the Z axis.
General syntax (in each WHEEL section):
```
Camber    -10
```
> The camber sign is automatically adjusted for left / right wheels. A negative
Camber value makes the bottom of the wheel farther out than the top.

## Special Effects

Enable Rotor, UFO or Mystery effect for your user car by adding new parameters
in the "Handling related stuff" in the parameters file. General syntax:
```
Flippable      FALSE  ; Rotor car effect
Flying         FALSE  ; Flying like the UFO car
ClothFx        FALSE  ; Mystery car cloth effect
```
Flying CPU cars will be able to steer in the air.

> Mystery car effect will also need modification to the car mesh to flag the
polygons that form the edges of the cloth.

To use Panga effect (i.e., head turning and ducking according to speed),
modify the SPINNER section. Further, you can use rotation only, ducking
(translation) only, or both. General syntax (in the SPINNER section):
```
Type           0  ; 1: Default rot, 2: Turn with steer,
                  ; 4: Translate with speed, 6: both 2 and 4
Trans          0.000000 3.000000 6.000000  ; Translation max
TransVel       0.001000  ; Velocity factor
```

> Unlike in legacy versions (eg., default Panga car) where `AngVel` was zero to
make the spinner turn with steer, now AngVel specifies the speed of turning,
so it should be non-zero. An AngVel value of zero denotes that the spinner
doesn't turn at all.

## CPU Selectability

Disable a user car for CPU selection by adding the `CPUSelectable` parameter in
the "frontend and selectability" section in the parameters file. General syntax:
```
CPUSelectable  FALSE
```

## Car Classes

As of version `20.0210a`, there are new Rating and Obtain values. Set Rating to
5 to add a Super Pro car. These are a class of cars faster than Pros. Set Obtain
to 5 to unlock the car upon completing Stunt Arena. General syntax:
```
Rating         5  ; Skill level (0-5)
Obtain         5  ; Obtain method (0-5 or -1)
```
## Weapon Offsets

As of version `21.0905a`, there are new parameters to define weapon generation
offsets for different classes of weapons. These go in the "Handling related stuff"
section in parameters.txt. The `Weapon` parameter is deprecated although it is
still accepted for backward compatibility. General syntax:
```
WeaponOffset1  0.000 -32.000 64.000  ; Offset for shockwaves and fireworks
WeaponOffset2  0.000 -16.000 80.000  ; Offset for water bombs
```

## Car Skins

Additional skins can be added for each car, selectable at the Car Preview screen.
Skins are detected based on a suffix system, i.e., if the TPAGE points to 'car.bmp',
then 'caror.bmp', 'car1.bmp' and 'car-bleh.bmp' are valid skins. Files that start
with the TCARBOX filename are skipped. The length of the skin suffix is currently
limited to 11 characters.

> As of version `18.1110a`, skins are randomized for CPU cars and in Random Cars mode.
