# Level Properties

<!-- toc -->

>  This feature has been introduced in version `17.1124a`.

Level properties are similar to _parameters.txt_ for cars. They allow you to 
customize certain aspects of your tracks:

- Materials  
    - Grip, roughness, hardness
    - Bump profile (corrugation)
    - Sounds
- Particle effects  
    - Particles used by weapons
    - Dust (appears when driving on some materials)
    - Particle trails
- Gravity
- Wind
- Pickups

RVGL looks for a _properties.txt_ file when loading a level. If such a file exists, 
the original materials and effects will be replaced with the ones in the file.

This can be used to tweak existing materials so they fit your track better or 
even to create entirely new materials like snow. 

## Sample Properties File
[**properties_default.txt**](properties_default.txt)  

This file contains all stock properties. You can use this to base your custom properties on.

## Materials
A material structure provides information about the surface cars drive on. 
Each material has a unique ID. In total, 64 different materials can be used, 
of which the first 27 are default materials. You can either override them or 
use the additional user slots.

### Default Materials
| ID   | Material    | ID   | Material          |
| :--- | :---------- | :--- | :---------------- |
| 0    | Default     | 14   | Pebbles           |
| 1    | Marble      | 15   | Gravel            |
| 2    | Stone       | 16   | Conveyor 1        |
| 3    | Wood        | 17   | Conveyor 2        |
| 4    | Sand        | 18   | Dirt 1            |
| 5    | Plastic     | 19   | Dirt 2            |
| 6    | Carpet Tile | 20   | Dirt 3            |
| 7    | Carpet Shag | 21   | Ice 2             |
| 8    | Boundary    | 22   | Ice 3             |
| 9    | Glass       | 23   | Wood 2            |
| 10   | Ice 1       | 24   | Conveyor Market 1 |
| 11   | Metal       | 25   | Conveyor Market 2 |
| 12   | Grass       | 26   | Paving            |
| 13   | Bump Metal  |      |                   |

To override a material, add the following to your _properties.txt_ file:
```
MATERIAL {
  ID               10               ; Material ID [0 - 63]
  Name             "Ice 1 Snow"     ; Display name
  Color            200 200 200      ; Display color

  Spark            true             ; Material emits particles
  Skid             true             ; Skid marks appear on material
  OutOfBounds      false            ; Not implemented
  Corrugated       true             ; Material is bumpy
  Moves            false            ; Moves cars like museum conveyors
  Dusty            true             ; Material emits dust

  Roughness        0.4              ; Roughness of the material
  Grip             0.4              ; Grip of the material
  Hardness         0.2              ; Hardness of the material

  DefaultSound     87               ; Sound when driving normally
  SkidSound        87               ; Sound when skidding [6:Normal, 7:Rough]
  ScrapeSound      5                ; Car body scrape [5:Normal]

  SkidColor        207 215 220      ; Color of the skid marks
  CorrugationType  2                ; Type of bumpiness
  DustType         2                ; Type of dust
  Velocity         0.0 0.0 0.0      ; Speed and direction cars are moved in
}
```
In this case, we're overriding the `ICE1` material (ID 10).

> If you're looking for materials to safely override: DIRT1, DIRT2 and DIRT3 
share the same values, there is no difference between them at all. CARPETSHAG and 
CARPETTILE also have the same values.

### Material Properties
| Property            | Description                              | Recommended Defaults                     |
| :------------------ | :--------------------------------------- | :--------------------------------------- |
| **ID**              | Unique ID number of the material.        | It's advised to override materials that aren't used in-game or have the same properties as others (DIRT2, DIRT3, CARPETTILE, PAVEMENT, etc.). |
| **Name**            | Display name, not used by the game.      | -                                        |
| **Color**           | Display color, not used by the game.     | -                                        |
| **Spark**           | The material emits particles that are defined by the assigned dust. | `true` or `false`.                       |
| **Skid**            | If `true`, cars leave skid marks on the ground. | `true` for most materials.               |
| **OutOfBounds**     | Not used in the game (yet).              | `false`.                                 |
| **Corrugated**      | Makes the surface bumpy.                 | `true` or `false`.                       |
| **Moves**           | Material moves the cars like the museum conveyors. | `true` or `false`.                       |
| **Dusty**           | Material generates dust when cars drive on it. | `true` or `false`.                       |
| **Roughness**       | Roughness of the material.               | Between 0.1 and 1.0.                     |
| **Grip**            | Grip of the material.                    | Between 0.1 and 1.0.                     |
| **Hardness**        | Hardness of the material.                | Between 0.1 and 1.0.                     |
| **DefaultSound**    | Persistent sound when cars are driving on the material. | -1 (disabled) or any sound ID.                 |
| **SkidSound**       | Sound when cars skid on the material (when the wheels lose traction or drift). | 6 (normal) or 7 (rough)                  |
| **ScrapeSound**     | Sound when cars scrape on the material (when the body touches the ground). | 5 (normal)                  |
| **SkidColor**       | Color of the wheel's skid marks.         | 50 50 50                                 |
| **CorrugationType** | ID of the corrugation to use for this material. *Corrugated* needs to be `true`. |                                          |
| **DustType**        | ID of the dust type to use for this material. *Dusty* needs to be `true`. |                                          |
| **Velocity**        | Velocity with which the cars are pushed around with if _Moves_ is `true`. | Multiples of 100.0 should be used.       |

### Sound IDs
Use these IDs for the _DefaultSound_, _SkidSound_ and _ScrapeSound_ properties.

> To use IDs other than `6` and `7` for normal and rough skid sounds, stock track 
sounds can be replaced. For example, _rattler.wav_ from Ghost Town could be replaced 
with a custom skid sound. To do so, place a file named _rattler.wav_ in your 
track's custom folder and use ID `89` in your _properties.txt_ file.

| ID   | Sound path              | ID   | Sound path                   |
| :--- | :---------------------- | :--- | :--------------------------- |
| 0    | **moto.wav**            | 53   | hood/**birds3.wav**          |
| 1    | **petrol.wav**          | 54   | hood/**dogbark.wav**         |
| 2    | **clockwrk.wav**        | 55   | hood/**kids.wav**            |
| 3    | **ufo.wav**             | 56   | hood/**sprink.wav**          |
| 4    | **honkgood.wav**        | 57   | hood/**tv.wav**              |
| 5    | **scrape.wav**          | 58   | hood/**lawnmower.wav**       |
| 6    | **skid_normal.wav**     | 59   | hood/**digger.wav**          |
| 7    | **skid_rough.wav**      | 60   | hood/**stream.wav**          |
| 8    | **pickup.wav**          | 61   | hood/**cityamb2.wav**        |
| 9    | **pickgen.wav**         | 62   | hood/**roadcone.wav**        |
| 10   | **shock.wav**           | 63   | garden/**tropics2.wav**      |
| 11   | **shockfire.wav**       | 64   | garden/**tropics3.wav**      |
| 12   | **electro.wav**         | 65   | garden/**tropics4.wav**      |
| 13   | **electrozap.wav**      | 66   | garden/**stream.wav**        |
| 14   | **firefire.wav**        | 67   | garden/**animal1.wav**       |
| 15   | **firebang.wav**        | 68   | garden/**animal2.wav**       |
| 16   | **balldrop.wav**        | 69   | garden/**animal3.wav**       |
| 17   | **ball.wav**            | 70   | garden/**animal4.wav**       |
| 18   | **hit2.wav**            | 71   | muse/**museumam.wav**        |
| 19   | **wbomb.wav**           | 72   | muse/**laserhum.wav**        |
| 20   | **wbombfire.wav**       | 73   | muse/**alarm2.wav**          |
| 21   | **wbombhit.wav**        | 74   | muse/**escalate.wav**        |
| 22   | **wbombbounce.wav**     | 75   | muse/**rotating.wav**        |
| 23   | **puttbang.wav**        | 76   | muse/**largdoor.wav**        |
| 24   | **fuse.wav**            | 77   | market/**aircond1.wav**      |
| 25   | **oildrop.wav**         | 78   | market/**cabnhum2.wav**      |
| 26   | **countdown.wav**       | 79   | market/**carpark.wav**       |
| 27   | **turbo.wav**           | 80   | market/**freezer1.wav**      |
| 28   | **servo.wav**           | 81   | market/**iceyarea.wav**      |
| 29   | **menunext.wav**        | 82   | market/**sdrsopen.wav**      |
| 30   | **menuprev.wav**        | 83   | market/**sdrsclos.wav**      |
| 31   | **menuupdown.wav**      | 84   | market/**carton.wav**        |
| 32   | **menuleftright.wav**   | 85   | ghost/**coyote1.wav**        |
| 33   | **lightflk.wav**        | 86   | ghost/**bats.wav**           |
| 34   | **boxslide.wav**        | 87   | ghost/**eagle1.wav**         |
| 35   | **starfire.wav**        | 88   | ghost/**minedrip.wav**       |
| 36   | **tvstatic.wav**        | 89   | ghost/**rattler.wav**        |
| 37   | **splash.wav**          | 90   | ghost/**townbell.wav**       |
| 38   | **honka.wav**           | 91   | ghost/**tumbweed.wav**       |
| 39   | **beachball.wav**       | 92   | ship/**intamb1.wav**         |
| 40   | **bottle.wav**          | 93   | ship/**seagulls.wav**        |
| 41   | toy/**piano.wav**       | 94   | ship/**shiphorn.wav**        |
| 42   | toy/**plane.wav**       | 95   | ship/**strmrain.wav**        |
| 43   | toy/**copter.wav**      | 96   | ship/**thunder1.wav**        |
| 44   | toy/**dragon.wav**      | 97   | ship/**wash.wav**            |
| 45   | toy/**creak.wav**       | 98   | roof/**traffic_mush.wav**    |
| 46   | toy/**train.wav**       | 99   | roof/**helicopter_loop.wav** |
| 47   | toy/**whistle.wav**     | 100  | roof/**siren_loop.wav**      |
| 48   | toy/**arcade.wav**      | 101  | roof/**wind_loop.wav**       |
| 49   | toy/**toybrick.wav**    | 102  | roof/**steamhiss_loop.wav**  |
| 50   | hood/**basketball.wav** | 103  | roof/**electric_hum.wav**    |
| 51   | hood/**birds1.wav**     | 104  | roof/**telemetry.wav**       |
| 52   | hood/**birds2.wav**     | 105  | roof/**air_con.wav**         |

---

## Corrugation
Corrugation structures define the bumpiness of the surface. There are 32 slots 
available, out of which 8 types are already defined by the game. Either override 
these default slots or use the additional user slots.

### Default Corrugation

| ID   | Corrugation | ID   | Corrugation |
| :--- | :---------- | :--- | :---------- |
| 0    | NONE        | 4    | CONVEYOR    |
| 1    | PEBBLES     | 5    | DIRT1       |
| 2    | GRAVEL      | 6    | DIRT2       |
| 3    | STEEL       | 7    | DIRT3       |

To override a corrugation, paste the following snipped into your _properties.txt_:

```
CORRUGATION {
  ID              1                             ; Corrugation ID [0-31]
  Name            "Name"                        ; Display name

  Amplitude       0.000000                      ; Amplitude of bumps (strength)
  Wavelength      0.000000 0.000000             ; Frequency of bumps
}
```

### Corrugation Properties
| Property       | Description                              | Recommended Defaults |
| :------------- | :--------------------------------------- | :------------------- |
| **ID**         | ID of the corrugation type.              | 0 - 31               |
| **Name**       | Name of the corrugation. Not used in-game. | -                    |
| **Amplitude**  | Strength of the corrugation (height of the bumps) | 0.0 - 5.0            |
| **Wavelength** | Frequency of the bumps                   | 0.0 - 100.0          |

---

## Dust
Dust structures are a set of two particles with a probability and variance. 
Materials emit dust when cars drive on them. There are 32 slots available, 
with 6 default dust types.

### Default Dust
| ID   | Dust   |
| :--- | :----- |
| 0    | NONE   |
| 1    | GRAVEL |
| 2    | SAND   |
| 3    | GRASS  |
| 4    | DIRT   |
| 5    | ROAD   |

To override a dust type, copy the following snippet into your _properties.txt_:

```
DUST {
  ID              1                             ; Dust ID [0 - 31]
  Name            "GRAVEL"                      ; Display name

  SparkType       4                             ; Particle to emit
  ParticleChance  0.600000                      ; Probability of a particle
  ParticleRandom  0.600000                      ; Probability variance

  SmokeType       29                            ; Smoke particle to emit
  SmokeChance     0.300000                      ; Probability of a smoke part.
  SmokeRandom     0.600000                      ; Probability variance
}
```

### Dust Properties
| Property           | Description                              | Recommended Defaults |
| :----------------- | :--------------------------------------- | :------------------- |
| **ID**             | ID of the dust type.                     | 0 - 31               |
| **Name**           | Display name of the dust type, not used in-game. | -                    |
| **SparkType**      | ID of the particle to emit.              | 0 - 63               |
| **ParticleChance** | The probability of a particle to be emitted. | 0.0 - 1.0            |
| **ParticleRandom** | Variance of the probability.             | 0.0 - 1.0            |
| **SmokeType**      | ID of the smoke particle to emit.        | 0 - 63               |
| **SmokeChance**    | The probability of smoke to be emitted.  | 0.0 - 1.0            |
| **SmokeRandom**    | Variance if the probability.             | 0.0 - 1.0            |

---

## Particles
Particles (also called sparks) can be emitted by materials, weapons, cars and other objects. 
There are 64 slots available, including 31 default types.

### Default Particles
| ID   | Particle      | ID   | Particle      |
| :--- | :------------ | :--- | :------------ |
| 0    | SPARK         | 16   | SMALLRED      |
| 1    | SPARK2        | 17   | EXPLOSION1    |
| 2    | SNOW          | 18   | EXPLOSION2    |
| 3    | POPCORN       | 19   | STAR          |
| 4    | GRAVEL        | 20   | PROBE_SMOKE   |
| 5    | SAND          | 21   | SPRINKLER     |
| 6    | GRASS         | 22   | SPRINKLER_BIG |
| 7    | ELECTRIC      | 23   | DOLPHIN       |
| 8    | WATER         | 24   | DOLPHIN_BIG   |
| 9    | DIRT          | 25   | SPARK3        |
| 10   | SMOKE1        | 26   | ROADDUST      |
| 11   | SMOKE2        | 27   | GRASSDUST     |
| 12   | SMOKE3        | 28   | SOILDUST      |
| 13   | BLUE          | 29   | GRAVELDUST    |
| 14   | BIGBLUE       | 30   | SANDDUST      |
| 15   | SMALLORANGE   |      |               |

To override a particle, copy the following snippet into your *properties.txt*:

```
SPARK {
  ID              0                             ; Particle ID [0 - 63]
  Name            "SPARK"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           false                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            1.000000 1.000000             ; Size of the particle
  UV              0.937500 0.000000             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           255 255 255                   ; Color of the particle

  Mass            0.100000                      ; Mass of the particle
  Resistance      0.020000                      ; Air resistance
  Friction        0.100000                      ; Sliding friction
  Restitution     0.500000                      ; Bounciness

  LifeTime        0.500000                      ; Maximum life time
  LifeTimeVar     0.050000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         0.000000                      ; Size variation
  GrowRate        0.000000                      ; How quickly it grows
  GrowRateVar     0.000000                      ; Grow variation

  TrailType       1                             ; ID of the trail to use
}
```

### Particle Properties
| Property          | Description                              | Recommended Defaults                     |
| :---------------- | :--------------------------------------- | :--------------------------------------- |
| **ID**            | ID of the particle.                      | 0 - 63                                   |
| **Name**          | Display name of the particle, not used in-game. | -                                        |
| **CollideWorld**  | The particle collides with the world (geometry from the .w file). | `true` or `false`                        |
| **CollideObject** | The particle collides with objects (cars and other level objects). | `true` or `false`                        |
| **CollideCam**    | The particle collides with the camera (currently not implemented). | `false`                                  |
| **HasTrail**      | The particle has a trail.                | `true` or `false`                        |
| **Field Affect**  | The particle is affected by force fields. | `true` or `false`                        |
| **Spins**         | The particle rotates.                    | `true` or `false`                        |
| **Grows**         | The particle grows bigger.               | `true` or `false`                        |
| **Additive**      | The particle is rendered additively: Black becomes transparent and bright colors are added to the underlying things. | `true` or `false`                        |
| **Horizontal**    | Instead of facing the camera, the particle is flat. | `true` or `false`                        |
| **Size**          | Size of the particle.                    | 1.0 1.0                                  |
| **UV**            | Top left UV-coordinate of the mapping. 0.0 0.0 is top left and 1.0 1.0 is bottom right. | -                                        |
| **UVSize**        | Width and height of the UV mapping (bottom right corner). | -                                        |
| **TexturePage**   | Texture the particle will be mapped to.  | `47`-`49` are fxpage\[1-3\]; `0`-`46` are track textures; -1 disables texturing. |
| **Color**         | RGB Color of the particle.               | 255 255 255                              |
| **Mass**          | Mass of the particle.                    |                                          |
| **Resistance**    | Air resistance of the particle (how slowly it travels through the ari). |                                          |
| **Friction**      | Friction when touching collideable objects. |                                          |
| **Restitution**   | Bounciness of the particle.              |                                          |
| **LifeTime**      | How long the particle lives.             | If this is too high, the creation of other particles might be prevented. |
| **LifeTimeVar**   | Variance of the life time.               |                                          |
| **SpinRate**      | The spin rate in radians per second.     |                                          |
| **SpinRateVar**   | Variation of the spin rate.              |                                          |
| **SizeVar**       | Initial size variation.                  |                                          |
| **GrowRate**      | How quickly the particle grows.          |                                          |
| **GrowRateVar**   | Variation of the grow rate.              |                                          |
| **TrailType**     | ID of the trail if enabled.              | 0 - 31                                   |


---

## Trails
Trails are similar to particles. They follow particles. They're used for fireworks and some sparks. 
There are 3 default trail types and a total of 32 slots for custom tracks.

### Default Trails
| ID   | Trail    |
| :--- | :------- |
| 0    | FIREWORK |
| 1    | SPARK    |
| 2    | SMOKE    |

To override a spark type, copy the following snippet into your *properties.txt*:

```
TRAIL {
  ID              1                             ; Trail ID [0 - 31]
  Name            "SPARK"                       ; Display name

  Fades           true                          ; Trail fades out
  Shrinks         true                          ; Trail shrinks
  Expands         false                         ; Trail expands
  UV              0.960938 0.000000             ; UV coord for trail segments
  UVEnd           0.960938 0.000000             ; UV coord for last segment
  UVSize          0.007812 0.062500             ; Width and height of both UV
  TexturePage     47                            ; Texture page
  Color           255 255 255 255               ; Alpha, Red, Green, Blue
  LifeTime        0.030000                      ; Maximum life time
  Width           1.000000                      ; Width of segments
  Length          2                             ; Number of segments
}
```

### Trail Properties
| Property     | Description                              | Recommended Defaults |
| :----------- | :--------------------------------------- | :------------------- |
| **ID**       | ID of the trail.                         | 0 - 31               |
| **Name**     | Display name of the trail, not used in-game. | -                    |
| **Fades**    | The trail fades out.                     | `true` or `false`    |
| **Shrinks**  | The trail segments get smaller.          | `true` or `false`    |
| **Expands**  | The trail segments get bigger.           | `true` or `false`    |
| **UV**       | Top left UV-coordinate of the mapping of the first trail segments. 0.0 0.0 is top left and 1.0 1.0 is bottom right. | -                    |
| **UVEnd**    | Top left UV-coordinate of the mapping of the last trail segment. 0.0 0.0 is top left and 1.0 1.0 is bottom right. |                      |
| **UVSize**   | Width and height of the UV mapping (bottom right corner). | -                    |
| **TexturePage** | Texture the segment will be mapped to.   | `47`-`49` are fxpage\[1-3\]; `0`-`46` are track textures; -1 disables texturing. |
| **Color**    | Alpha, Red, Green, Blue color of the trail. | 255 255 255 255      |
| **LifeTime** | How long the trail segments live.        |                      |
| **Width**    | Width of the trail segments.             |                      |
| **Length**   | Number of trail segments.                | 1 - 16               |


---

## Gravity
This section allows you to modify the behavior of the global gravitational field 
for your track. To customize the level's gravity, copy the following snippet 
into your *properties.txt*:

> As of version `19.1230a`, the ROCK and ROCKTYPE level \*.inf keys are deprecated. 
New tracks should set these properties in the *properties.txt* GRAVITY section 
instead.

```
GRAVITY {
  Magnitude       2200.0                        ; Magnitude of gravity
  Direction       0.000000 1.000000 0.000000    ; Direction of gravity
  Dampening       0.000000                      ; Dampening effect
  Rockiness       0.000000 0.000000             ; Rocky effect parameters
                  0.000000 0.000000
  RockyType       0                             ; Type of rocky effect [0 - 1]
}
```

### Gravity Properties
| Property      | Description                               | Recommended Defaults |
| :------------ | :---------------------------------------- | :------------------- |
| **Magnitude** | Strength of the gravity field.            | 250.0 - 5000.0       |
| **Direction** | Direction of action of the gravity field. | 0.0 1.0 0.0          |
| **Dampening** | Dampening effect of gravity.              | 0.0 - 100.0          |
| **Rockiness** | Parameters for the rocky ship effect: four values for the X and Z magnitude and X and Z time scale. | If magnitudes are non-zero, time must be non-zero. |
| **RockyType** | Type of rocky effect: 0 for ship, 1 for water surface. | 0 - 1   |


---

## Wind
This section allows you to modify the behavior of the Wind force field types in 
your track. To customize the level's wind properties, copy the following snippet 
into your *properties.txt*:

```
WIND {
  Priority        2                             ; Field priority [0 - 2]
  TimeMin         5                             ; Min update rate
  TimeMax         30                            ; Max update rate
  Expands         true                          ; Size expands
}
```

### Wind Properties
| Property      | Description                               | Recommended Defaults |
| :------------ | :---------------------------------------- | :------------------- |
| **Priority**  | Wind field priority. 1 affects both objects and cars; 2 affects objects only. | 0 - 2 |
| **TimeMin**   | Minimum time before wind changes direction. | 5.0                  |
| **TimeMax**   | Maximum time before wind changes direction. | 30.0                 |
| **Expands**   | Whether the field expands to cover the entire world. | If true, the field is always Box shaped. |


---

## Pickups
This section allows you to modify the behavior of pickups in the level, including 
spawn count. To customize pickups, copy the following snippet into your *properties.txt*:

```
PICKUPS {
  SpawnCount      5 2                           ; Initial and per-player count
  EnvColor        255 255 128                   ; Color of shininess (RGB)
  LightColor      128 96 0                      ; Color of light (RGB)
}
```

### Pickup Properties
| Property       | Description                               | Recommended Defaults |
| :------------- | :---------------------------------------- | :------------------- |
| **SpawnCount** | Number of pickups to spawn: two values for the initial count and per-player count. | 5 2     |
| **EnvColor**   | Color of shininess effect on the pickup.  | 255 255 128          |
| **LightColor** | Color of the light under the pickup.      | 128 96 0             |


