# Advanced

<!-- toc -->

## Dev Mode

Below, you can find technical information that is intended for modders, track
creators and car creators.

Working with tracks and cars is more convenient in *Dev mode*. This is a
special mode that enables several advanced features for content creation. To
access this mode, run RVGL with the `-dev` command line.

>  Enabling Dev mode flags your cars as CHT. Avoid using it for normal gameplay.

### Options

> These options are available only in Dev mode.

**Calculate Car Stats**: This game mode, accessible from the *Start Race* menu,
allows you to calculate stats of your car for frontend display. The top speed,
acceleration, weight and transmission are calculated. Page Up / Page Down allow
you to change the car and restart the test run.

**Save Current Car Info**: Use this in-game menu option to re-save the current car's
*parameters.txt* info. The saved file is up to date with all supported entries. Original
cars are re-signed to ensure that they are not treated as CHT.

**Edit Mode**: Select the edit mode for the in-game editor from the *Start Race* menu.
As of version `20.0210a`, it is also possible to change the edit mode on the fly from
the in-game menu.

### Commands

> These commands work in Dev mode, some commands may also work by manually entering
MAKEITGOOD edit mode or other appropriate cheat codes.

**Generate AI Lines from Ghost Path**: You must be in AI Nodes edit mode in combination
with Time Trial mode. You must also have completed a best lap so you have a valid
ghost car. Assuming you're using the Modern Editor, press G to generate a path from
the ghost car. Then press Shift + G to update the racing lines, and Ctrl + Shift + G
to update the overtaking lines.

**Generate Curves for AI Lines**: You must be in AI Nodes edit mode. This functionality
helps you generate smooth curves at turns and chicanes. You select 3 consecutive nodes,
one before the turn, one at the turn itself and one after the turn. Assuming you're
using the Modern Editor, hold down the N key and select the 3 nodes. These are control
points from which the curve is generated. Adjust the curve by moving the middle node's
racing / overtaking lines. When you're satisfied, press Shift + N to update the racing
lines, and Ctrl + Shift + N to update the overtaking lines.

**Preview Objects**: In Objects edit mode, press Ctrl + P to preview object animations
instantly, then use the same command to get back to editing. The same shortcut is
used to preview / refresh visiboxes.

**Cheat Codes**: Certain cheats such as TVTIME (F5, F6 cameras) or CHANGELING
(Page Up / Page Down to change car) are enabled in Dev mode. It is also possible
to disable previously enabled cheat codes by adding an '!' at the end.

**Show Camera Position**: Press Right Shift + F9 to show position and direction of the
camera. This can help set the required StartPos and StartRot values in the level \*.inf
file, by placing the camera at the start line and noting the camera values.

**Camera Commands**: Use Caps Lock to toggle the camera movement speed and the zoom
speed when using scroll wheel. Use Z for zooming in and X for zooming out. To tilt the
camera, use Alt + Mouse X (horizontal mouse movement). Use C to reset the camera zoom
and tilt.

**Keep game running in background**: Press Shift + F9 to keep the game running when
it's not focused. This is similar to the `-nopause` command line.

**Hide HUD and Menus**: Shift + F5 hides the HUD. Shift + Ctrl + F5 hides the in-game
menu. Shift + F7 hides race results. These options can help with taking screenshots
of your car or track.

**Full Track Reload**: Restarting the level does a full reload in Dev mode.

**Instant Car Refresh**: Press Ctrl + R to refresh the current car. This allows you to
quickly make changes to the car parameters and data and view those changes in-game.
This works both at the Frontend selection screen and in-game.

**Camera Edit Mode**: Press Ctrl + C to interactively edit the Hood / Rear camera
parameters. Use Numpad keys to move the camera and hold down Ctrl for more precise
movement. Right Ctrl + F11 and Right Shift + F11 cycle through the available Follow
and Attached cameras, respectively.

**Network Ping**: In Multi-Player sessions, ping and traffic information can be checked
by pressing Ctrl + P to enable the debug display. This works in Dev mode or with
`-showping` command line.

## Modern Editor

As of version `19.0907a`, there is support for a modern editor scheme and alternate
camera controls. These options can be enabled from the *[Editor]* section in *rvgl.ini*.

- *LegacyCameraControls*: Set this to 0 to use modern camera navigation controls.
      The changes include a more *standard* WSAD set (where W = forward, S = back,
      Q = up, E = down), and Y-axis movement that follows the mouse motion instead
      of being inverted.
- *LegacyEditorControls*: Set this to 0 to enable the modern editor workflow.
      The supported edit modes are documented below.

### Common Tools

The modern editor contains new tools that perform Translation, Rotation and Scaling
in a much more intuitive way. These manipulations can be axis-constrained (one,
two or all axes) and set to occur in World or Object space.

- 1 = Translate
- 2 = Rotate
- 3 = Scale
- I = Isolate
- Alt = Space (World or Object)
- Ctrl + S = Save file

### Pos Nodes

- Ctrl + Left Mouse = Add new node
- Left Mouse on node = Select node
- Left Mouse Drag on node = Move node
- Select node + Shift + Right Mouse on another = Connect nodes (order matters)
- Select node + Shift + Right Mouse on connected node = Disconnect nodes
- Select node + Shift + Ctrl + Left Mouse = Add new node (chain linking)
- Ctrl + Left Mouse over connection line = Inserts a node inbetween
- Delete = Remove node (Selected first, then if any under cursor)

*Hold Alt = Changes to "Alt" mode.*

- Select node + Shift + Right Mouse = chain linking

### AI Nodes

*Shares similar controls with the Pos Nodes editor.*

- Ctrl + Left Mouse = Add new segment
- Left Mouse on segment = Select segment
- Left Mouse Drag on segment = Move segment
- Select segment + Shift + Right Mouse on another = Connect segments (order matters)
- Select segment + Shift + Right Mouse on connected segment = Disconnect segments
- Select segment + Shift + Ctrl + Left Mouse = Add new segment (chain linking)
- Ctrl + Left Mouse over connection line = Inserts a segment inbetween
- Delete = Remove segment (Selected first, then if any under cursor)
- Drag white line on selected segment = modifies racing line
- G = Generate ghost path (in Time Trial mode)
- Shift + G = Update racing line from ghost path
- Ctrl + Shift + G = Update overtaking line from ghost path
- N + Left Mouse = Select nodes for curve generation
- Shift + N = Update racing line from generated curve
- Ctrl + Shift + N = Update overtaking line from generated curve

*Hold Alt = Changes to "Alt" mode.*

- Select node + Shift + Right Mouse = chain linking
- Allows modifying the overtaking line (Drag pink line on selected segment)
- Clicking on nodes of the segment toggles a wall depending on the side

### Camera Nodes

*Shares similar controls with the Pos Nodes editor.*

- Insert = Add new node
- Left Mouse on node = Select node
- Left Mouse Drag on node = Move node
- Select node + Shift + Right Mouse on another = Connect nodes (order matters)
- Select node + Shift + Right Mouse on connected node = Disconnect nodes
- Delete = Remove node (Selected first, then if any under cursor)

### Track Zones

- Insert = Add new zone
- Left Mouse on zone = Select zone
- Left Mouse Drag on zone = Move zone (in Translate mode)
- Left Mouse Drag on zone side = Scale side (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping zones
- Delete = Remove selected zone
- N = Toggle AI nodes

### Force Fields

- Insert = Add new field
- Left Mouse on field = Select field
- Left Mouse Drag on field = Move field (in Translate mode)
- Left Mouse Drag on field side (box) = Scale side (in Scale mode)
- Left Mouse Drag on field (sphere) = Scale inner radius (in Scale mode)
- Shift + Left Mouse Drag on field (sphere) = Scale outer radius (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping fields
- Delete = Remove selected field
- Shift + D = Duplicate field
- Enter = Reset data entry (when applicable)
- Numpad Enter = Toggle data entry (when applicable)
- Numpad Period = Mod type (Zone or Direction)
- Numpad 0 = Reset orientation
- Alt + Numpad 0 = Reset direction (in Direction mode)
- G = Set gravity field (magnitude)
- Ctrl + G = Set gravity field (direction)
- Alt + G = Set anti-gravity field

### Triggers

- Insert = Add new trigger
- Left Mouse on trigger = Select trigger
- Left Mouse Drag on trigger = Move trigger (in Translate mode)
- Left Mouse Drag on trigger side = Scale side (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping triggers
- Delete = Remove selected trigger

### Visiboxes

- Insert = Add new visibox
- Left Mouse on visibox = Select visibox
- Left Mouse Drag on visibox = Move visibox (in Translate mode)
- Left Mouse Drag on visibox side = Scale side (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping visiboxes
- Delete = Remove selected visibox

### Instances

- Insert = Add new instance
- Left Mouse on instance = Select instance
- Left Mouse Drag on instance = Move instance (in Translate mode)
- Shift + Left Mouse = Cycle through overlapping instances
- Delete = Remove selected instance
- Shift + D = Duplicate instance

### Objects

- Insert = Add new object
- Left Mouse on object = Select object
- Left Mouse Drag on object = Move object (in Translate mode)
- Shift + Left Mouse = Cycle through overlapping objects
- Delete = Remove selected object
- Shift + D = Duplicate object

### Lights

- Insert = Add new light
- Left Mouse on light = Select light
- Left Mouse Drag on light = Move light (in Translate mode)
- Left Mouse Drag on shadow box side = Scale side (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping lights and shadow boxes
- Space = Change light type
- Delete = Remove selected light
- Shift + D = Duplicate light

### Portals

- Insert = Add new portal
- Left Mouse on portal = Select portal
- Left Mouse Drag on portal = Move portal (in Translate mode)
- Left Mouse Drag on portal side = Scale side (in Scale mode)
- Shift + Left Mouse = Cycle through overlapping portals
- Delete = Remove selected portal
