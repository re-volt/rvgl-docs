# Custom Cups

<!-- toc -->

>  This feature has been introduced in version `20.1230a`.

You can add new championship cups to the game, just like additional cars and tracks. 
Each cup is defined in a text file (like `cupfile.txt`) placed in the `cups` 
folder. The available cups are listed at the Championship selection screen.

> The following cupfile names are reserved:
> - *bronze*
> - *silver*
> - *gold*
> - *platinum*

## Sample Cup File
```
Name        "I/O Super Pro Cup"
Difficulty  4
Obtain      0

NumCars     10  ; Number of cars [4 - 16]
NumTries    5   ; Number of retry attempts
QualifyPos  3   ; The minimum finish position to qualify per race
UnlockPos   1   ; The minimum overall podium position to unlock the next cup

Classes     0,0,0,0,0,9       ; Number of CPU cars picked from each rating
Points      10,8,6,5,4,3,2,1,0,0  ; Points obtained for each position


; Stages [0 - 15]: level laps mirrored reversed

STAGE 0     roof1 2 false false
STAGE 1     schoolslite 2 false false
STAGE 2     cliffside 1 false false
STAGE 3     fair1 5 false false
STAGE 4     wildland 3 true false
STAGE 5     ranch 2 false true
STAGE 6     fair2 3 false true
STAGE 7     schools 5 false true
```

## Cup File Properties
| Property       | Description                              |
| :------------- | :--------------------------------------- |
| **Name**       | Name of the cup as displayed in-game.    |
| **Difficulty** | Difficulty level of the cup (see [Difficulty](tracks.html#difficulty)). This affects the difficulty level of the CPU cars. |
| **Obtain**     | Unlock method of the cup (see [Obtain](tracks.html#obtain)). |
| **NumCars**    | Number of cars. Accepted values are 4-16, default being 8. |
| **NumTries**   | Number of retry attempts, default being 3. |
| **QualifyPos** | The minimum finish position to qualify per race (1-16), default being 3. |
| **UnlockPos**  | The minimum overall podium position to unlock the next cup (1-3), default being 1. This setting only works on stock cups and is ignored in custom ones. |
| **Classes**    | Number of CPU cars picked from each rating. The values must be *comma-separated* and add up to one less than the number of cars. |
| **Points**     | Points obtained for each position, list of *comma-separated* values. |
| **STAGE**      | Defines one or more stages of the cup. There can be up to 16 stages in a cup. The *STAGE* keyword is followed by the stage number (0-15), folder name of the track, mirrored and reversed specifiers (true/false). |
