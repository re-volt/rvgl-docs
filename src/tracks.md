# Tracks

<!-- toc -->

Customize user-made tracks through new entries in the level \*.inf file and the
extensive *custom* folder support.

## End Position
There is a new ENDPOS key for use with Sprint tracks. This is only applicable when GAMETYPE is 4.
Upon changing either the STARTPOS or ENDPOS, the AI Nodes and Pos Nodes must be re-saved.

```
ENDPOS          -17784 -8846 -90895  ; End position coordinates
ENDPOSREV       -17784 -8846 -90895
```

> ENDPOSREV is a deprecated keyword. The new way to specify values for reversed mode
is by placing a dedicated reversed \*.inf. See [Reversed Mode Customization](#reversed-mode-customization).

*Added in version `19.0907a`*.

## Starting Grids
New 3-car wide STARTGRID with type numbers 4 and 5 have been added for use in custom tracks.

```
STARTGRID       4  ; Grid type (0 - 5, type 2 reserved for frontend)
STARTGRIDREV    4
```

> STARTGRIDREV is a deprecated keyword. The new way to specify values for reversed mode
is by placing a dedicated reversed \*.inf. See [Reversed Mode Customization](#reversed-mode-customization).

The behavior when repositioning directly to the start grid (eg., in Time Trial mode) can
be customized. Use the STARTGRIDRESET key and set it to either 0 or 1.

0. Car is reset to the node above the start grid. This is useful when the start grid is actually a platform that moves up.
1. Car is actually reset to the start grid (the default).

```
STARTGRIDRESET  1  ; Grid reset behavior (0: node reset, 1: grid reset)
```

## Difficulty
A custom track's difficulty can be set by adding the following line to the track's \*.inf file:

```
DIFFICULTY      1  ; Display difficulty (0:Unknown, 1:Easy, 2:Normal, 3:Hard, 4:Extreme)
```

*Added in version `17.1009a`*.

## Obtain
A custom track can have unlockability conditions that work similar to stock content. Set this
using the OBTAIN keyword. Just as a car's Obtain value is linked to its Rating, a track's
OBTAIN value is linked to its DIFFICULTY.

```
OBTAIN          0  ; Obtain level (-1:Never, 0:Default, 1...5)
```

The default OBTAIN behavior for custom tracks is to unlock the track and all its variants
immediately. For stock tracks, the standard unlockability rules apply. Rest of the options
are documented below.

1. Unlock by winning Championship in the appropriate tier.
2. Unlock by finding Practice mode stars in the appropriate tier.
3. Unlock by beating Time Trial challenges in the appropriate tier.
4. Unlock by winning Single Races in the appropriate tier.
5. Unlock by collecting all stars in Stunt Arena.

For OBTAIN level 1 and above, unlocking a track does not automatically unlock its variants.
These are unlocked by beating the track's challenge times, specified with the CHALLENGE
keyword. If no challenge time is found, the variants are unlocked immediately.

- Reversed mode is unlocked by beating the Normal challenge time.
- Mirrored mode is unlocked by beating the Reversed challenge time.
- Reversed Mirrored mode is unlocked by beating the Mirrored challenge time.

*Added in version `20.0430a`*.

## Challenge Times
User-made tracks can now set challenge times to be beaten in Time Trial with new
level \*.inf keywords CHALLENGE and CHALLENGEREV. General syntax:

```
CHALLENGE       0 54 0  ; Time in <min sec ms>
CHALLENGEREV    0 52 0
```

> CHALLENGEREV is a deprecated keyword. The new way to specify values for reversed mode
is by placing a dedicated reversed \*.inf. See [Reversed Mode Customization](#reversed-mode-customization).

*Added in version `18.0330a`*.

## Sweep Camera
The starting grid zoom camera can be enabled for a custom track by setting the
following line to the track's \*.inf file:

```
SWEEPCAM        1  ; Sweep camera (0:Disabled, 1:Enabled)
```

*Added in version `20.0210a`*.

## Texture Properties
Set various per-texture properties for the level using the TEXTUREPROPS keyword, followed by
four values specifying the texture index (0-63), mipmapping (0:disable / 1:enable), wrap mode
(0:clamp / 1:repeat / 2:mirrored-repeat) and colorkeying (0:disable / 1:auto). General syntax:

```
TEXTUREPROPS    3 1 0 1  ; Index - mipmaps - wrap mode - colorkey
```

For example, use 'TEXTUREPROPS 1 1 1 0' to make texture page B use mipmapping, support
mapping outside the boundaries and disable colorkeying (no pure-black transparency).

> The colorkey attribute is used to hint that the texture doesn't require alpha
testing. It doesn't guarantee that alpha testing is actually disabled.

*Added in version `18.0428a`*.

## Rock Type
There is a new level \*inf keyword `ROCKTYPE`, which can be 0 or 1. The default 0
can be used for ship levels, 1 can be used for water surfaces (Boat tracks).

```
ROCKTYPE    0  ; Rocking type (0:Ship, 1:Water)
```

> Deprecated. New tracks should set rockiness properties in the *properties.txt*
GRAVITY section instead. See the [Gravity](#gravity) section.

*Added in version `16.0420a`*.

## Custom Music
To play custom music in your level instead of the default soundtrack, use the
MUSIC entry in the level .inf file. General syntax:

```
MUSIC    <Path to music file (or) folder>
```

As of version `19.0330a`, the path accepts either a single file or a folder containing
music tracks. If a folder path is provided, the game selects a music track from the
folder to be played at random. Supported formats are Ogg (recommended), Flac, MP3,
WAV and MIDI (experimental).

> This keyword was previously called MP3, which is now deprecated. The MUSIC
keyword is recommended for new tracks.

> If both MUSIC and REDBOOK lines are present in the inf, MUSIC has higher
priority for custom tracks.

> Experimental MIDI support was added in version `18.1020a`. This requires a
[SoundFont](https://github.com/FluidSynth/fluidsynth/wiki/SoundFont) placed in
the *soundfonts* folder, with the name *default.sf2*. MIDI playback is only
supported on Windows and Linux.

## Game Modes

**Practice**: User tracks can be played in Practice mode. Add a single star
object for this mode. When the player finds and catches it, the progress will
be saved.

**Battle / Stunt / Frontend / Sprint**: These special type of tracks can be created
with the help of a new GAMETYPE entry in the track .inf file. General syntax:

```
GAMETYPE    1  ; Track type (0:default, 1:battle, 2:stunt, 3:frontend, 4:sprint)
```

Add several stars for Battle Tag mode and upto 64 stars for Stunt Arena. The
progress is saved for Stunt Arena tracks.

> Support for Frontend track type was added in version `19.0120a`. See the
[Frontend Properties](#frontend-properties) section.

> Support for Sprint track type was added in version `19.0907a`. This requires
a valid ENDPOS to be specified.

**Reversed Mode**: Support for Reversed mode can be added by including a "reversed"
folder in your track folder, containing the needed files (see the default levels
for example).

> The direction of AI Nodes and Pos Nodes can be automatically reversed using
special commands in the appropriate edit modes.

## File Formats

These options let you select the format to be used when saving files in edit
mode, in cases where multiple formats are supported (typically a legacy format
for backwards compatibility and newer, modern formats). When not specified,
files are saved in the modern format by default.

```
CAMFORMAT    1  ; Camera nodes format (0:Legacy, 1:Modern)
FLDFORMAT    1  ; Force fields format (0:Legacy, 1:Modern)
```

## Object Thrower

As of version `20.0210a`, the Object Thrower trigger supports extended options.

- Flag low: Thrower object ID.
- Flag high: The lap to trigger in (0 = any lap, -1 = last lap, n = nth lap).

## Level Textures

As of version `19.0120a`, RVGL supports up to 64 level textures with an extended
naming scheme. File names along with their texture ID are listed in the table
below.

> Intermediate textures should not be skipped. For example, textures K followed
by M is an invalid sequence. The first 10 textures (A-J) are, however, always loaded.

| ID   | File name               | ID   | File name                    |
| :--- | :---------------------- | :--- | :--------------------------- |
| 0    | level**a**.bmp          | 32   | level**ga**.bmp              |
| 1    | level**b**.bmp          | 33   | level**ha**.bmp              |
| 2    | level**c**.bmp          | 34   | level**ia**.bmp              |
| 3    | level**d**.bmp          | 35   | level**ja**.bmp              |
| 4    | level**e**.bmp          | 36   | level**ka**.bmp              |
| 5    | level**f**.bmp          | 37   | level**la**.bmp              |
| 6    | level**g**.bmp          | 38   | level**ma**.bmp              |
| 7    | level**h**.bmp          | 39   | level**na**.bmp              |
| 8    | level**i**.bmp          | 40   | level**oa**.bmp              |
| 9    | level**j**.bmp          | 41   | level**pa**.bmp              |
| 10   | level**k**.bmp          | 42   | level**qa**.bmp              |
| 11   | level**l**.bmp          | 43   | level**ra**.bmp              |
| 12   | level**m**.bmp          | 44   | level**sa**.bmp              |
| 13   | level**n**.bmp          | 45   | level**ta**.bmp              |
| 14   | level**o**.bmp          | 46   | level**ua**.bmp              |
| 15   | level**p**.bmp          | 47   | level**va**.bmp              |
| 16   | level**q**.bmp          | 48   | level**wa**.bmp              |
| 17   | level**r**.bmp          | 49   | level**xa**.bmp              |
| 18   | level**s**.bmp          | 50   | level**ya**.bmp              |
| 19   | level**t**.bmp          | 51   | level**za**.bmp              |
| 20   | level**u**.bmp          | 52   | level**ab**.bmp              |
| 21   | level**v**.bmp          | 53   | level**bb**.bmp              |
| 22   | level**w**.bmp          | 54   | level**cb**.bmp              |
| 23   | level**x**.bmp          | 55   | level**db**.bmp              |
| 24   | level**y**.bmp          | 56   | level**eb**.bmp              |
| 25   | level**z**.bmp          | 57   | level**fb**.bmp              |
| 26   | level**aa**.bmp         | 58   | level**gb**.bmp              |
| 27   | level**ba**.bmp         | 59   | level**hb**.bmp              |
| 28   | level**ca**.bmp         | 60   | level**ib**.bmp              |
| 29   | level**da**.bmp         | 61   | level**jb**.bmp              |
| 30   | level**ea**.bmp         | 62   | level**kb**.bmp              |
| 31   | level**fa**.bmp         | 63   | level**lb**.bmp              |

## Advanced Customization

Support for advanced custom content in user tracks. This includes:

- Animated objects
- Skymap, clouds, other graphics
- Sound effects
- Loadscreen image
- HUD, menu and font

This works by replacing default (stock) data with custom content. When a model,
sound, texture, etc. has to be loaded, the file is first searched inside the
`custom` sub-folder located in the current track's folder.

The only thing to do to add custom content in your track is to create a `custom`
folder inside the track's directory and place the custom files inside, with the
same name as the stock content you want to replace.

Here is the list of the customizable files:

- Skyboxes: sky_bk.bmp to sky_tp.bmp
- All the .wav files (sfx).
- All Objects added in Edit mode: the .m files and their associated .hul or .ncp files, if any.
- All track meshes: the .prm files and their associated .ncp files, if any.
- All track files: .inf, .fob, .fld, .fan, .fin, .cam, .pan, .tri, .taz, .por, .w, vis, .rim, .ncp, .lit and textures.
- Special textures: trolley.bmp, sun.bmp, dragon.bmp, water.bmp, clouds.bmp.
- Load screen images: loadlevel1.bmp to loadlevel4.bmp, loadfront1.bmp to loadfront4.bmp.
- Effect pages: fxpage1.bmp to fxpage3.bmp.
- Car textures: shadow.bmp, carbox1.bmp to carbox5.bmp.
- Text and HUD: font.bmp, spru.bmp, spruimage.bmp, acclaim.bmp, loadinga.bmp to loadingc.bmp.
- Countdown models: gogo.m and go1.m to go3.m.

This is an exhaustive list. The support for some formats was added to be able to
release a track compatible with legacy Re-Volt versions. This makes it possible
to include eg., a simplified .fob file in the main track folder and a custom .fob
file placed in the "custom" folder.

In Reverse mode, the custom version of the files in the `reversed` folder have
to be placed inside the `custom\reversed` folder to be detected.

## Reversed Mode Customization

>  This feature has been introduced in version `18.0720a` and subsequently
updated in version `18.0731a`.

The ability to customize levels for the *reversed* variant has vastly improved. All
supported level files and a few level graphics can now have reversed variants placed
in the `reversed` or `custom\reversed` folder.

Below is a list of newly supported files that can have reversed variants:

- w
- vis
- rim
- ncp
- lit
- fob
- fld
- por
- inf
- properties.txt
- custom\\sky_*.bmp
- custom\\loadlevel*.bmp
- custom\\loadfront*.bmp
- custom\\fxpage*.bmp
- custom\\\<level\>*.bmp

If one of these files is not present in the `reversed` or `custom\reversed` folder,
the game will fall back to the forward version of the file. Note that fallback support
is only provided for newly added formats. There is no fallback functionality for file
formats that were already supported. Specifically, a reversed variant must be provided
for these formats:

- cam
- fan
- fin
- pan
- taz
- tri

All supported `<level>.inf` keywords can be customized for reversed mode by placing a
`reversed\<level>.inf` file. When a `reversed\<level>.inf` file is found, the
STARTPOS, STARTROT, STARTGRID and CHALLENGE keys are taken from that file instead of
the REV variants present in the main `<level>.inf` file.


## Limitations

**World Polys**: Each world mesh supports up to 65535 polys. Large worlds should be
chopped into several meshes or "cubes" to avoid hitting this limit.

**Collision Polys**: The maximum polys supported by the world \*.ncp file is limited
to 65535. However, it is possible to add any number of additional instance \*.ncp
files, i.e., the total number of world + instance collpolys can exceed 65535.

**Camera Nodes**: RVGL gives you the option to save the \*.cam file in either the
old format or the new format with support for coordinate ranges beyond (-32768, 32768).

**Force Fields**: RVGL gives you the option to save the \*.fld file in either the
old format or the new format with extended set of options.

**File Limits**: Following are the limiting values for various level files:

- AI Nodes, Pos Nodes, Cam Nodes (4096)
- Track Zones, Force Fields, Triggers, Visiboxes, Portals (1024)
- Lights, Objects, Instance Models (1024)
- Pickups (192 normal + 64 clones = 256)
- Level Textures, Visibox IDs (64)
