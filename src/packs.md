# Content Packs

<!-- toc -->

>  This feature has been introduced in version `20.1230a`.

RVGL has a flexible content management system that works similar to tools like 
[Mod Organizer](https://github.com/ModOrganizer2/modorganizer). Content can be 
placed in multiple folders and enabled selectively through a packlist file.

Each pack is a folder containing some content. Any file that is part of the game 
data counts for pack content. The player profile, i.e., the *profiles*, *times*, 
*replays* and *cache* folders are not loaded from packs.

When a packlist is active, only content from the listed packs are enabled. The 
result is as though each of those pack folders were extracted one after the other, 
saying yes to overwrites (so files belonging to a later listed pack has priority). 
The game sees a merged view of all the pack content.

## Features
The system is quite powerful and gives you a lot of freedom in managing your 
content. Here are a couple of things you can do:

- Keep mods in separate folders and enable / disable them without affecting the main installation.
- Manage multiple content sets (eg, for online events) and switch between them.
- No files are actually copied or moved, which means zero overhead.
- Save in-game edits to a separate `local` folder.

## Managing Packs
Packs and packlists are loaded from the `packs` folder. The default packlist is 
called `default.txt`. This is active when no other packlist is used. A sample 
packlist file `sample.txt` is included with RVGL. Make a copy of it and name it 
`default.txt`.

Place all your packs in the `packs` folder, then edit the default packlist and list 
packs that must be enabled. Packs are read-only by default. When a packlist is active, 
all in-game edits made with the editor get saved to a special pack called `local`. 
To learn about the available options when editing a packlist, see the next section.

The command line `-packlist <name>` can be used to select a packlist different 
from the default packlist. This causes the packlist at `packs\<name>.txt` to be 
loaded.

## The Packlist Format
A packlist is a simple text file that lists the enabled packs, one on each line. 
Suppose your `packs` folder contains the following packs:
- addon1
- addon2
- special
- local

Then, your packlist can be written like this:

**Example #1**
```
default
addon1
addon2
local
```
This enables your root content, then applies *addon1*, *addon2* and your local edits 
on top of it, in the listed order. In this example, the *special* pack remains 
disabled.
- The keyword `default` refers to the main installation or root folder. Unless this 
is listed, your root content will not be loaded.
- The special pack `local` holds your in-game edits, made either through the editor 
or with an option such as Save Car Info. If you don't list this pack, your edits 
still get saved, but they won't show up in-game.
- Pack names with spaces must be covered with quotes. Lines starting with a 
semi-colon are ignored.

**Example #2**
```
default
addon1
"my pack"
;this is a comment
```

- Packs are read-only by default. An asterisk next to the pack name marks it as 
writable. A packlist can have at most one writable pack. The default writable pack, 
as we've seen above, is `local`. To ensure that your in-game edits show up correctly, 
list the writable pack last.

**Example #3**
```
default
addon1
addon2
myedits *
```
The above example enables the root content and applies *addon1*, followed by 
*addon2*. In-game edits go into the *myedits* pack, which is applied last.

**Example #4**
```
default *
```
The above example has a single entry that enables the root content, and makes it 
writable. This is identical to the default behavior when no packlist is active.
