# Platform Specific

<!-- toc -->

## Windows

> The recommended way to install RVGL is using [RVGL Launcher](https://re-volt.gitlab.io/rvgl-launcher/),
a cross-platform launcher and package manager. Pre-built full game packages are
also available at [Re-Volt I/O](https://re-volt.io/downloads/game).

Updates for RVGL are available either as an executable installer or 7-zip archive.
Using the installer is recommended as it updates necessary registry entries and
folder permissions, and optionally creates a Desktop shortcut.

The Windows versions support DirectPlay lobby applications like GameRanger or
GameSpy Arcade. Support for this is provided in *dplobby_helper.dll*. Running the setup
also ensures that the game is properly detected by lobby applications.

> Running RVGL in Administrator mode is not recommended. Instead, place the game
in a locally accessible folder (such as My Documents) or a drive other than the
system drive if possible.

### Direct3D Backend

On Windows systems with poor or non-existent OpenGL support, RVGL can take advantage of
Google's [ANGLE project](https://en.wikipedia.org/wiki/ANGLE_%28software%29) which provides
a compliant OpenGL ES implementation based on Direct3D 9 or 11. To use this support,
edit *rvgl.ini* and set `Shaders = 1` to enable the shader-based renderer, and set
`Profile = 3` for ANGLE Direct3D.

You can verify whether the Direct3D backend is being used by checking the Video Info
section in *profiles\\re-volt.log*. Below is a sample output:
```
GL Vendor: Google Inc.
GL Renderer: ANGLE (AMD Radeon™ R4 Graphics Direct3D11 vs_5_0 ps_5_0)
GL Version: OpenGL ES 3.0 (ANGLE 2.1.0.54118be67360)
```

### Discord URI

RVGL registers the custom URI `discord-472158403830218762://` for Discord Rich Presence
integration. The registration is done only once during the RVGL setup. To customize the
URI and set various launch parameters, edit the registry key at:
```
HKEY_CURRENT_USER\Software\Classes\discord-472158403830218762\shell\open\command
```
The `"%1"` at the end of the path is a required parameter. The default URI can be
re-registered by running RVGL with the `-register` command line.

## GNU/Linux

> The recommended way to install RVGL is using [RVGL Launcher](https://re-volt.gitlab.io/rvgl-launcher/),
a cross-platform launcher and package manager. Pre-built full game packages are
also available at [Re-Volt I/O](https://re-volt.io/downloads/game).

> The instructions below cover manual installation steps. There is no need to do
these steps when installing through RVGL Launcher.

Before starting the game for the first time, run the `setup` script included in
the package. The setup converts game files to lower case, adds necessary file
permissions, and places an application launcher at `~/.local/share/applications`.

Launch the game using the `rvgl` launch script. The script auto-detects your system
architecture and launches the appropriate (32-bit or 64-bit) executable. It also resolves
dependencies by using libraries bundled with the game in place of missing ones. This
allows the game to run without manual configuration on a wide range of systems, as long
as the OpenGL drivers, SDL2 and SDL2_image packages are installed.

All game data must be in *lower case*. To repair file names after installing custom
content, run the `fix_cases` script. Starting from version `18.1020a`, these scripts
are require Bash 4.

### Required Packages

```bash
# Debian / Ubuntu
sudo apt install libsdl2-2.0-0 libsdl2-image-2.0-0
sudo apt install libopenal1 libenet7 libunistring2
sudo apt install libjpeg8 libpng16-16 libtiff5 libwebp6
sudo apt install libvorbisfile3 libflac8 libmpg123-0 libfluidsynth1

# Arch Linux
sudo pacman -S sdl2 sdl2_image
sudo pacman -S openal enet libunistring
sudo pacman -S libjpeg libpng libtiff libwebp
sudo pacman -S libvorbis flac mpg123 fluidsynth

# Fedora
sudo yum install SDL2 SDL2_image
sudo yum install openal enet libunistring
sudo yum install libjpeg libpng libtiff libwebp
sudo yum install libvorbis flac mpg123 fluidsynth
```

### Discord URI

RVGL registers the custom URI `discord-472158403830218762://` for Discord Rich Presence
integration. The registration is done only once during the RVGL setup. To customize the
URI and set various launch parameters, edit the Desktop entry at:
```
~/.local/share/applications/discord-472158403830218762.desktop
```
The `"%u"` at the end of the path is a required parameter. The default URI can be
re-registered by running RVGL with the `-register` command line.

## macOS

RVGL has been available on macOS since version `21.0125a`. It's distributed in
the form of universal binaries and has native support for 64-bit Intel and M1
(ARM64) architectures.

These are the earliest devices that RVGL runs on:
```
iMac (Mid 2007 or later)
MacBook (13-inch Aluminum, Late 2008), (13-inch Polycarbonate, Early 2009 or later)
MacBook Pro (13-inch, Mid 2009 or later), (15-inch or 17-inch, Mid/Late 2007 or later)
MacBook Air (Late 2008 or later)
Mac Mini (Early 2009 or later)
Mac Pro (Early 2008 or later)
Xserve (Early 2009)
```

The DMG available from the RVGL project page requires original game data. This is
not included in the bundle itself. However, a full game bundle is also available
from [Re-Volt I/O](https://re-volt.io/downloads/game).

If you got the minimal DMG from the RVGL project page, you must copy game files
to `~/Library/Application Support/RVGL` (see the next section on locating the data
path), but take care that you do *not* overwrite any of the existing files in the
folder (i.e., say NO to replace).

### Data path on macOS

To see bundled resources, right-click the app and select `Show Package Contents`.
This takes you to a path similar to:
```
/Applications/RVGL.app/Contents/Resources
```
Content is not loaded directly from the bundle path. Instead, files are copied
to your data path on first run, and after each update. This ensures that content
is not lost when the app is updated or replaced.

Game content, save games and configuration files are stored at one place in a
user-specific folder. Look for:
```
/Users/<user-name>/Library/Application Support/RVGL
```
You can place your custom content and content packs in here.

### OpenGL Support

The OpenGL version available on macOS depends on the profile used by the game:
- When a Compatibility profile is used, the available OpenGL version is 2.1.
- When a Core profile is used, the available OpenGL version is 4.1.

When RVGL is installed and run for the first time, it automatically detects the
best available OpenGL version and sets the profile accordingly.

To learn more about OpenGL profiles and how they can be configured, see the
[OpenGL Profiles](general.html#opengl-profiles) section.

> As of version `21.0905a` high DPI rendering is supported and enabled by
default. On supported devices, this causes the render resolution to be scaled
2x the screen resolution. This can be performance intensive, but reducing the
Antialias level should help in most cases. You can also disable this by setting
`HighDPI = 0` in *rvgl.ini*.

### Launch Parameters

To start the game with command line arguments, you have one of these options:
- Use the terminal: `cd` to `RVGL.app` and run `./Contents/MacOS/RVGL -params`
- Create a shell script that does the above.
- Set up an alias for RVGL.

## Android

RVGL requires at least Android 3.1 with an OpenGL ES 2.0 capable GPU. It's
compatible with the ARMv7 32-bit processor, ARMv8 64-bit processor, x86 and
x86_64 architectures.

The APK available from the RVGL project page requires original game data. This
is not provided in the APK itself. However, a full game build is also available
from [Re-Volt I/O](https://re-volt.io/downloads/game).

### Data path on Android

The following locations are checked in order to find the game files:
```
/sdcard/RVGL
/storage/sdcard0/RVGL
/storage/sdcard1/RVGL
/storage/emulated/legacy/RVGL
/storage/emulated/0/RVGL
/storage/emulated/1/RVGL
/storage/extSdCard/RVGL
```

Running the app will automatically create the RVGL folder at the first available
location. If you got the minimal APK from the RVGL project page, extract game
files into this folder, but take care that you do *not* overwrite any of the
existing files in the folder (i.e., say NO to replace).

### Generated files

+ A file named *rvgl_version.txt* is created on first run. Deleting it
    will force the game to re-extract the latest assets from the apk.
+ The log file is generated at *profiles/re-volt_log.txt*.
+ Running the RVGL Controller Map app (see below) saves controller mappings
    to *profiles/gamecontrollerdb.txt*.
+ Advanced settings can be configured from *profiles/rvgl.ini*. To display the
    frame rate, set *ShowFPS* to 1. To set the device orientation, set
    *Orientation* to a value between 0-5.

### Improving performance

The Shader Edition might benefit from level world (\*.w) files optimized with
[WorldCut](http://jigebren.free.fr/games/pc/re-volt/#worldcut) to have larger
sized meshes. As of version `18.1126a`, optimized level files are included in
the app package.

### Gameplay tips

+ *Flip Car*: When the car is upside down, tap anywhere at the centre of the
    screen (eg., on the car itself) to flip the car.
+ *UI navigation*: Tap anywhere outside the menu box to go back to the
    previous screen, or use the Back button on the device.

### Native Multisampling

Native multisampling is very fast on embedded GPUs and enjoys wider support:

- 2x or 4x native multisampling can be enabled on embedded GPUs with virtually no performance loss.
- It's supported on low end hardware like the Adreno 203.
- It can be used with either the shader-based GLES2+ renderer or the legacy GLES1 renderer.

However, native multisampling cannot be configured in-game from Render Settings. It must be set
directly in *rvgl.ini* through the *Antialias* key. It's usually safe to set it to 2 or 4. The
behavior is undefined when an unsupported value is used. The currently used Antialias level
can still be checked from Render Settings.

The alternative is to use Frame Buffer Objects for multisampling, which can be quite slow on
embedded GPUs. Because of the performance benefits, FBO is disabled by default on Android.

### Controller Map

Available as a separate optional download, the
[RVGL Controller Map](https://rvgl.org/downloads/rvgl_controllermap.apk) app
allows you to generate mappings for your controllers interactively. Once configured,
the mappings are automatically saved to *profiles/gamecontrollerdb.txt* and is
used by the game upon next start.

### Launch Parameters

As of version `20.0210a`, it is possible to launch the app from the adb shell with
optional command line arguments. These are set through a string extra called `args`.
```
am start -n com.rvgl.rvgl/.RVGLActivity -e args "-nointro -lobby 1.1.1.1"
```
