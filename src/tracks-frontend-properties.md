# Frontend Properties

<!-- toc -->

>  This feature has been introduced in version `19.0120a`.

Frontend properties are similar to _properties.txt_. They allow you to customize 
certain aspects of your custom frontend, i.e., Camera Positions.

RVGL looks for a _frontend.txt_ file when loading a frontend. If such a file exists, 
the original frontend properties will be replaced with the ones in the file.

## Sample Frontend File
[**frontend_default.txt**](frontend_default.txt)  

This file contains all stock frontend properties. You can use this to base your 
custom properties on.

## Camera Positions
Provides information about the camera position and direction at each menu screen. 
In total, 25 camera views are available.

### Default Camera Positions
| ID   | Camera       | ID   | Camera            |
| :--- | :----------- | :--- | :---------------- |
| 0    | INIT         | 13   | DINKYDERBY        |
| 1    | START        | 14   | RACE              |
| 2    | CAR_SELECT   | 15   | BESTTIMES         |
| 3    | CAR_SELECTED | 16   | INTO_TRACKSCREEN  |
| 4    | TRACK_SELECT | 17   | SUMMARY           |
| 5    | MULTI        | 18   | PODIUMSTART       |
| 6    | TROPHY1      | 19   | PODIUM            |
| 7    | TROPHY2      | 20   | PODIUMVIEW1       |
| 8    | TROPHY3      | 21   | PODIUMVIEW2       |
| 9    | TROPHY4      | 22   | PODIUMVIEW3       |
| 10   | TROPHYALL    | 23   | PODIUMLOSE        |
| 11   | OVERVIEW     | 24   | TROPHYUSER        |
| 12   | NAME_SELECT  |      |                   |

To override a camera, add the following to your _frontend.txt_ file:
```
CAMPOS {
  ID     0                                      ; Camera ID
  Name   "INIT"                                 ; Display name

  Eye    76.500000 -79.300000 -4462.600000      ; Camera position
  Focus  109.500000 -155.300000 -3466.100000    ; Camera focus
}
```
In this case, we're overriding the `INIT` camera (ID 0).

### Frontend Camera Properties
| Property            | Description                              |
| :------------------ | :--------------------------------------- |
| **ID**              | Unique ID number of the camera position. |
| **Name**            | Display name, not used by the game.      |
| **Eye**             | Position of the camera.                  |
| **Focus**           | Look direction of the camera.            |



