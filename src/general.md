# General

<!-- toc -->

## Features at a Glance

- A new renderer based on programmable shaders.
- Support for various platforms (Windows, Linux, macOS, Android) across multiple
    architectures (x86, x86_64, armhf, aarch64).
- Wide screen, Full HD, 4K resolutions and multi-monitor support.
- Unicode support, additional languages and International Keyboard Layouts support.
- Improved and re-designed high resolution game font.
- Additional content from the Dreamcast version brought to PC and Android.
    - Rooftops level and additional cars from the DC version (available as a separate
      optional download). BigVolt and BossVolt are added to the stack of carboxes.
    - Additional Gallery pages from the DC version.
    - Dreamcast mode: Use the DC layout for the Championships and car selection screen.
- Multi-Player lobby launching, Spectator mode and Discord Rich Presence integration.
- Lookback button from the console versions and a new *Hood View*.
- Play against up to 15 other opponents.
- Support for 2 to 4 player Split-Screen Multi-Player mode.
- Support for modern game controllers, mouse controls and touch controls on Android.
- Support for 3D surround sound. Surround speakers or headphones with virtual
    surround capability are required.
- Support for original soundtrack playback, ripped from PC, Dreamcast or PSX versions.
- Improved AI: Better wall avoidance and steering correction, intelligent oversteer
    correction, smarter recovery from crashes, and better use of special routes.
- Customization: Level properties, car shadows and box art, other extensive
    features for custom content creation. The *Random Cars* and *Random Tracks*
    settings take custom content into account.
- Car skins: Choose between alternate skins when available at the car preview screen.
- Custom cups: Create your own championship cups.
- Custom frontends: Select between user-made frontends (i.e., menu screens)
    from *Options -> Select Frontend*.
- Content search: Start typing the name of the content (car, track or cup) at the
    selection screens and jump to it.
- Content packs: Manage custom content packs and enable/disable them individually.

## Languages

Language files are placed in the game's *strings* folder. Text files placed in
the folder are automatically detected by the game. Additional details about the
language can be specified using an appropriate *escape sequence*.

- Use `\LN<Language Name>` to specify the localized name of the language. This
    is the name of the language displayed in-game.
- Use `\LC<Language Locale>` to specify the two-letter
    [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) code for
    the language. The locale affects things like case mapping. For example, `i`
    is capitalized to `İ` in Turkish (`tr`) locale and `I` in other locales.

These details are typically added to the end of the language file, though they
can occur anywhere in the file. Below is a sample taken from *french.txt*:
```
\LNFrançais
\LCfr
```

As of version `18.1110a`, RVGL supports and uses UTF-8 encoding for the language
files and other text based configuration files. For more detailed information about
Unicode support, see the section on [Internationalization](#internationalization).

The following languages are currently included: *Basque, Czech, Dutch, English,
French, German, Hungarian, Italian, Lithuanian, Polish, Portuguese, Romanian,
Serbian, Slovak, Slovenian, Spanish, Swedish* and *Turkish*.

> You can contribute your translations to the
[RVGL Localization Project](https://forum.rvgl.org/viewtopic.php?f=8&t=122),
or send a pull request on [GitLab](https://gitlab.com/re-volt/rvgl-assets).

## Profiles

Save files are located in the *profiles* folder. RVGL has a global settings file
named "rvgl.ini". Profile specific settings are saved to "profile.ini" within
the player's profile folder (eg., *profiles\\marv\\profile.ini*). Profile names are
capitalized in-game. The player's profile folder also contains game progress.

Best times and lap records of all players are saved in the *times* folder. Replays
are saved in the *replays* folder.

> As of version `18.1110a`, player names and profile names accept international
characters through Unicode support. Because of the UTF-8 encoding used, however,
this can limit the number of typeable characters.

## Soundtrack

Original Soundtrack (Dreamcast version) is available along with the game
assets from [Re-Volt I/O](https://re-volt.io/downloads/misc).

If you have a retail Re-Volt CD, you can rip the soundtrack yourself. RVGL
supports soundtrack from the PC, Dreamcast and PSX versions.

+ **PC / DC version**: Rip the 14 audio tracks from the CD to one of the supported
    formats (Ogg, Flac, MP3 or WAV). Name them sequentially from `track02[.ogg]`
    to `track15[.ogg]` (file extension may vary), and place them in the game's
    *redbook* folder.

+ **PSX version**: The soundtrack is present on the disc as `TRACK1.DA`. Convert it
    to Ogg and name the file `track1.ogg`, then place it in the game's *redbook*
    folder. Note that Ogg is the only format supported in this case.

> The PSX soundtrack is better suited for gameplay, as it has tracks stitched
together for seamless playback, and the Dreamcast soundtrack is considered the
most complete version.

## Dreamcast Mode

Additional content from the Dreamcast version is available in RVGL as an optional
download. This adds the Rooftops level and 14 additional cars to the game. The
Championship cups and carboxes layout are preserved from the original PC version.

If you grew up playing Re-Volt on Dreamcast, you now have the option to enable
*Dreamcast layout* for the Championship cups and the carboxes at the selection screen.

+ **Step 1**: Ensure that you've downloaded and installed the
    [Dreamcast Pack](https://rvgl.org/downloads/rvgl_dcpack.7z).
+ **Step 2**: Enable Dreamcast mode in your profile.
    + Navigate to your profile within the *profiles* folder and open *profile.ini*.
    + Locate the key `CupDC` and set it to 1, then save it.
+ **Step 3**: Enable Dreamcast carboxes layout.
    + Navigate to *levels\\frontend\\custom\\dc* in your game folder.
    + Copy the contents of the folder (i.e., the files *frontend.fob* and
        *frontend.fin*) to one level above it (to *levels\\frontend\\custom*).

> Some of the additional cars have alternate skins based on the PSX version.
These can be selected from the Car Preview screen.

## Multi-Player

RVGL supports up to 16 players in Single Race or Battle Tag mode, plus 8
additional slots for spectators, resulting in a total of 24 connections in a
direct Peer-to-Peer network. Spectators can join once slots free up.

RVGL utilizes UDP hole punching to connect players behind NAT. In most cases, no
port forwarding is required for clients. The host is required to forward port
2310 (UDP) to be able to accept incoming connections.

> **Hybrid P2P**: In cases where a direct connection is not possible between players,
RVGL is capable of linking the affected players together through multicasting (i.e.,
messages between these players pass through the host). This happens automatically
and ensures the best possible connection between players.

**Waiting Room commands**:

+ Up / Down arrow keys scroll through the player list. When there are more than
    12 players, Page Up / Page Down jumps to the prev / next page.

+ Host can use *Ctrl + D* to kick (disconnect) a player. *Ctrl + Shift + D* kicks the
    player and blocks them from connecting again during the session. *Ctrl + S* puts
    the player in Spectator mode and *Ctrl + A* puts the player in Active mode.

+ Host has their Public IP address displayed at the top right corner. This can be
    copied to the clipboard with *Ctrl + C*. *Ctrl + V* pastes clipboard content in
    chat messages and when joining a game (in the Host Computer field).

**In-Game commands**:

+ The In-Game lobby is accessible with the TAB key.
+ Type a quick message while racing by pressing the F12 key.
+ Up / Down at the race results screen is used to spectate unfinished players.

The *Back To Lobby* in-game menu option can be used to go back to Frontend
without quitting the online session. This allows players to change their name
and car. It also allows the host to re-launch the session with new settings,
such as different number of laps. *This feature was added in version `18.1110a`*.

**Discord Rich Presence**:

Multi-Player games can be announced and joined directly from the Discord app. Metadata
like the game cover-art and gameplay state is advertised in a Discord user's profile.
To be able to launch games directly from Discord, a custom URI is registered during
the RVGL setup. To manually register the URI, run RVGL with the `-register` command line.
To customize the URI further, see the [Platform specific](os-specifics.html) sections.
The [Discord Desktop client](https://discordapp.com/download) is required.
*This feature was added in version `18.0731a`*.

> Visit the community's official [Discord server](https://discord.gg/NMT4Xdb).

> The Windows versions support DirectPlay lobby applications like GameRanger or
GameSpy Arcade. Support for this is provided in *dplobby_helper.dll*.

## Options

+ **Game Settings**:
    + *Multiplayer CPU*: Enable or disable CPU cars in Multi-Player mode.
    + *Show Ghost*: Enable or disable the ghost car in Time Trial.
    + *Split Times*: Choose between Global and Per-Rating split times in Time Trial.
    + *Camera View*: Choose between the available camera modes (Android only).

+ **Video Settings**:
    + *V-Sync*: Enable or disable sync to vertical blank. Supported values are On, Off
        and Adaptive. Adaptive V-Sync is supported on certain Windows and Linux systems.
    + *Generate Mipmaps*: Mipmap levels can be generated automatically at loading
        time. This is best used in conjunction with Anisotropic texture filtering.
    + *Anisotropy*: Support for enabling Anisotropic texture filtering from the
        Render Settings. This is only applicable when Texture Filter is set to Linear.
    + *Antialias*: Support for enabling Multisample anti-aliasing (MSAA) from the Render Settings.
    + *Water Ripples*: Enable or disable the water ripples effect in eg., Botanical Garden.
        This can help improve performance with some Intel cards.

+ **Controller Settings**:
    + *Controller Slot*: Select the player for which controls are to be configured (1-4).
    + *Button Opacity*: When touch controls are used, change the visibility level of
        the virtual buttons (Android only).
    * *Force Feedback*: Enable or disable Force Feedback or Haptic support for each controller.

+ **In-Game Options**:
    + *Next Track*: Start a different track without quitting to Frontend.
    + *Save Replay*: Save the race replay. View saved replays from *Options -> View Replay*.

## Advanced Options

These options are accessible only by editing the game configuration files.

> **Log files**: The game log is generated at *profiles\\re-volt.log*. Running
the `alsoft_log` script generates an OpenAL info log at *profiles\\alsoft.log*.

These options can be changed in **rvgl.ini** and apply to all profiles:

+ **Video section**:
    + *Profile* to set the OpenGL profile to be used. For more information,
        check the [Renderer](#renderer) section.
    + *Shaders* to enable / disable the shader-based rendering engine. For more
        information, check the [Renderer](#renderer) section.
    + *ShaderLights*, *ShaderShadows*, *ShaderEffects* to configure the number of lights,
        shadow boxes and mesh effects that affect each mesh (0-16). Only applicable in shader mode.
    + *SortLevel* to switch between complex and minimal translucency sorting schemes.
        Only applicable in shader mode.
    + *Threaded* to enable / disable multithreaded OpenGL support for loading.
    + *Orientation* to set the device orientation (0-5, or -1). Supported values
        are: Auto (-1), Landscape (0), Portrait (1), Sensor Landscape (2), Sensor
        Portrait (3), Reverse Landscape (4), Reverse Portrait (5).
    + *EffectFlag* to enable / disable certain visual effects like mesh deformation and the
        bomb scorch effect that can be taxing on low end systems or mobiles.
    + *CompressTextures* to enable / disable texture compression if available.
    + *LimitFPS* to limit the frame rate with vsync off. Set the preferred frame rate
        or set it to zero to disable limiting. The minimum supported limit is 15.
    + *LimitLatency* to reduce input lag (low latency mode). This can have a negative
        impact on performance. Set to 1 to use standard latency control mechanism, or
        2 to use Fence Sync Object when available.
    + *Compositor* to enable / disable Desktop compositing on Linux. Enabling this
        allows smoother transition between RVGL and other windows, at the cost of
        slightly reduced performance.
    + *HighDPI* to enable / disable high DPI rendering on macOS.

+ **Network section**:
    + *LocalPort* forces a specific port to be used when connecting to an online
        session. If you have trouble seeing other players online, you should forward
        a specific port (UDP) to your system and then set LocalPort accordingly.
        In most cases, it's safe and recommended to leave this at 0.
    + *DiscordRPC* toggles Discord Rich Presence support, allowing you to completely
        disable it by setting it to 0.

+ **Editor section**:
    + *LegacyCameraControls* to switch camera controls between modern and legacy modes.
        See the [Modern Editor](advanced.html#modern-editor) section.
    + *LegacyEditorControls* to switch between modern and legacy editor workflow.
        See the [Modern Editor](advanced.html#modern-editor) section.

+ **Misc section**:
    + *UseProfiles* to enable / disable multiple profiles support. When disabled, the
        *DefaultProfile* is loaded automatically, or if none exists, a profile is created
        automatically.

These options are changed in **profile.ini** and are specific to a player profile:

+ **Game section**:
    + *CatchUp* to enable / disable CPU cars "rubberbanding".
    + *CupDC* to enable / disable Dreamcast mode for the Championship cups. Requires the DC Pack to be installed.
    + *Difficulty* to adjust the AI difficulty level in single races (0-3).
    + *FinalLapMsg* to enable / disable the _Final Lap!_ message.
    + *AnimateRearView* to enable / disable the rear view transition animation.
    + *RearViewType* to choose between dynamic (0) and classic (1) rear view.
    + *FinalCam* to enable / disable the special finish camera at race end.
    + *WeaponCam* to enable / disable the weapon in-set camera used for fireworks.
    + *RandomSkins* to enable / disable the randomization of CPU and Random Cars skins.
    + *NLaps* to change the number of laps (goes up to 255).
    + *NCars* to change the number of cars (can be set to 1 to race alone; in this
        case, race wins are not counted as progress).

## Keyboard Commands

**In-set Cameras**: F1 changes camera view (can be configured from *Controller Settings*).
In Replay and Spectator modes, it can be used to disable the dynamic view changes and lock
the camera to a specific view. F2 and F3 toggle rear view and follow view cameras. F4
cycles between players in the follow view.

**Toggle Fullscreen**: F11 toggles between Fullscreen and Windowed mode.

**Screenshot**: F8 automatically saves a screenshot. The screenshots are timestamped
and saved in the *profiles* folder as PNG files.

## Content Search

Find / filter cars and tracks at the selection screens using the Keyboard. The
search is automatically activated on typing.

**Search Starts with...**
Type eg., "TOY", and it selects the first car whose name starts by TOY (likely the
TOYECA). As long as this search is active you can navigate with the Left / Right keys
amongst all cars whose name starts with TOY. To cancel search, press Esc. If navigation
is impossible or the search has no match, it produces a blocking sound.

**Search Contains...**
If you want to search for a word included in the name but not necessarily at the beginning,
just press the "\*" key, and now if you type SLUG it will find all occurrences that
match the wildcard search "\*SLUG\*", for example Phat Slug.

**Commands**

+ *Home*: Jump to the first available car. A second press jumps to the first user car.
+ *End*: Jump to the last stock car. A second press jumps to the last user car.
+ *Page Up / Page Down*: Jump 10 cars at a time.

## Controllers

+ Controller *hot-plugging* is supported.
+ The D-Pad is used for menu navigation when supported.
+ Menu forward and back are set to buttons A and B by default on supported controllers.
+ Controller axes and buttons are properly supported for menu navigation.

The SDL2 *GameController* API is used to provide support for modern game controllers
like the Xbox 360 and PS3 Controllers. This means Triggers can be selected
along with other axes. Axis and Button names are displayed in the *Controller Config*
menu for these devices. A GameController mapping database is included that
supports various controllers "out of the box".

> Controller GUIDs and mappings are logged at game start. This can be useful for adding
your own GameController mapping in case one is not available for your controller. Custom
mappings are sourced from *profiles\\gamecontrollerdb.txt*.

> GameController mappings can be generated interactively. For Windows / Linux, use the
[SDL2 Gamepad Tool](http://generalarcade.com/gamepadtool/) by General Arcade. For Android,
use the [RVGL Controller Map](https://rvgl.org/downloads/rvgl_controller_map.apk)
app (see the [Android](os-specifics.html#controller-map) section).

## Renderer

As of version `18.0428a`, there is support for a new rendering engine based on programmable
shaders, targeting OpenGL / OpenGL ES 2.0 and above. This shader-based renderer is expected to
have significantly improved performance on modern GPUs and better power efficiency on
laptops and mobile devices.

As of version `18.1020a`, the shader-based render is used by default. To switch back to
the fixed pipeline renderer, edit *rvgl.ini* and set `Shaders = 0`. This is applicable
to both Desktop and Mobile versions.

**OpenGL Functionality**:

RVGL has optional support for new OpenGL functionality that takes advantage of newer hardware.
Some of these options are only applicable when the shader-based renderer is active. Support
for these features can be verified from the "Video Info" section in *profiles\\re-volt.log*.

- *Uniform Buffer Objects*: Buffer-backed uniform blocks are used to store and update shader state
    which can provide better stability and improved performance when rendering scenes with large
    number of meshes. Requires OpenGL 3.0, GLES 3.0 or extensions. The feature can be toggled
    with *EnableUBO* in *rvgl.ini*.
- *Separate Shader Objects*: Shader programs for the vertex and fragment shading stages are generated
    separately, enabling better reuse of shader code. Requires OpenGL 4.1, GLES 3.1 or extensions.
    The feature can be toggled with *EnableSSO* in *rvgl.ini*.
- *Frame Buffer Objects*: Antialias support is implemented through Frame Buffer Objects (i.e., the
    application maintains its own frame buffers rather than rendering directly to the window buffers).
    To use a native multisampled window, set *EnableFBO* to 0 in *rvgl.ini*. This is particularly
    useful for embedded GPUs (see the [Android](os-specifics.html#native-multisampling) section).

**OpenGL Profiles**:

Advanced users can select one of the supported OpenGL Profiles through the `Profile` key
in *rvgl.ini*. Some of these profiles are only applicable when the shader-based renderer
is active. The following values are supported:

0. *Default profile*: Initialize a backwards compatible OpenGL profile.
1. *Core profile*: Initialize a 3.2+ Core profile. Only applicable in *shader mode*.
    In some cases, driver support might be better with a core profile.
2. *ES profile*: Initialize an OpenGL for Embedded Systems profile. Requires a mobile
    GPU or driver support for ES emulation. Depending on whether *shader mode* is active,
    this will initialize either GLES 1.1 or GLES 2.0+.
3. *D3D profile*: On Windows, initialize an OpenGL ES 2.0+ profile using the bundled
    [ANGLE](https://en.wikipedia.org/wiki/ANGLE_%28software%29) implementation, based on
    the Direct3D backend. Only applicable in *shader mode*.

## Internationalization

As of version `18.1110a`, RVGL uses Unicode (UTF-8) encoding for the game internals and all
text content. The game is also capable of reading Unicode filenames through UTF-8 on Linux
and Android, and Wide Character support on Windows.

> Text files that are not in valid UTF-8 encoding are assumed to be ASCII
([Windows-1252](https://en.wikipedia.org/wiki/Windows-1252)) and converted
internally to UTF-8.

The re-designed game font provides support for various commonly used Latin alphabet
sets and diacritics. The following Latin alphabets are considered fully supported:
*English, French, German, Italian, Spanish, Portuguese, Dutch, Danish, Norwegian,
Swedish, Finnish, Estonian, Hungarian, Basque, Romanian, Polish, Lithuanian, Latvian,
Czech, Slovak, Slovenian, Croatian, Bavarian, Welsh, Livonian, Turkish* and *Vietnamese*.

Further, the following alphabets are supported through standard romanization
schemes: *Indic* ([IAST](https://en.wikipedia.org/wiki/International_Alphabet_of_Sanskrit_Transliteration),
[NLK](https://en.wikipedia.org/wiki/National_Library_at_Kolkata_romanisation) and
[ISO 15919](https://en.wikipedia.org/wiki/ISO_15919)), *Cyrillic*
([ISO 9](https://en.wikipedia.org/wiki/ISO_9#ISO_9:1995,_or_GOST_7.79_System_A) and
[language-specific standards](https://en.wikipedia.org/wiki/Romanization_of_Cyrillic)),
*Greek* ([ISO 843](https://en.wikipedia.org/wiki/ISO_843)), *Georgian*
([ISO 9984](https://en.wikipedia.org/wiki/Romanization_of_Georgian)), *Armenian*
([ISO 9985](https://en.wikipedia.org/wiki/Armenian_alphabet#Transliteration)),
*Arabic* ([ISO 233](https://en.wikipedia.org/wiki/ISO_233)) and *Hebrew*
([ISO 259](https://en.wikipedia.org/wiki/ISO_259)).

Below is a complete list of international characters supported by the game:

- `Grave: à À c̀ C̀ è È f̀ F̀ g̀ G̀ ì Ì k̀ K̀ ǹ Ǹ ò Ò p̀ P̀ s̀ S̀ t̀ T̀ ù Ù ẁ Ẁ ỳ Ỳ`
- `Acute: á Á ć Ć é É ǵ Ǵ í Í j́ J́ ḱ Ḱ ĺ Ĺ ń Ń ó Ó ṕ Ṕ ŕ Ŕ ś Ś ú Ú ẃ Ẃ ý Ý ź Ź`
- `Circumflex: â Â ĉ Ĉ ê Ê î Î k̂ K̂ l̂ L̂ n̂ N̂ ô Ô ŝ Ŝ û Û ŵ Ŵ ŷ Ŷ ẑ Ẑ`
- `Circumflex + Grave: ầ Ầ ề Ề ồ Ồ`
- `Circumflex + Acute: ấ Ấ ế Ế ố Ố`
- `Circumflex + Tilde: ẫ Ẫ ễ Ễ ỗ Ỗ`
- `Circumflex + Macron: û̄ Û̄`
- `Circumflex + Dot Below: ậ Ậ ệ Ệ ộ Ộ`
- `Circumflex + Hook Above: ẩ Ẩ ể Ể ổ Ổ`
- `Trema: ä Ä c̈ C̈ ë Ë ï Ï ö Ö ẗ T̈ ü Ü ẅ Ẅ ÿ Ÿ z̈ Z̈`
- `Trema + Acute: ḯ Ḯ ÿ́ Ÿ́`
- `Trema + Macron: ǟ Ǟ`
- `Tilde: ã Ã ẽ Ẽ ĩ Ĩ ñ Ñ õ Õ ũ Ũ ỹ Ỹ`
- `Tilde + Macron: ȭ Ȭ`
- `Cedilla: ç Ç ḑ Ḑ ȩ Ȩ ģ Ģ ḩ Ḩ ķ Ķ ļ Ļ ņ Ņ ş Ş ţ Ţ`
- `Cedilla + Breve: ḝ Ḝ`
- `Ring: å Å ů Ů`
- `Ring Below: l̥ L̥ r̥ R̥`
- `Ring Below + Macron: l̥̄ L̥̄ r̥̄ R̥̄`
- `Caron: ǎ Ǎ č Č ď Ď ě Ě ǧ Ǧ ǐ Ǐ ǰ J̌ ǩ Ǩ ľ Ľ ň Ň ř Ř š Š ť Ť ž Ž`
- `Caron + Cedilla: ž̧ Ž̧`
- `Caron + Comma: ž̦ Ž̦`
- `Caron + Comma Above: č̕ Č̕`
- `Double Acute: a̋ A̋ ő Ő ű Ű`
- `Breve: ă Ă c̆ C̆ ĕ Ĕ ğ Ğ ĭ Ĭ m̆ M̆ n̆ N̆ ŏ Ŏ r̆ R̆ ŭ Ŭ z̆ Z̆`
- `Breve Below: ḫ Ḫ`
- `Breve + Grave: ằ Ằ`
- `Breve + Acute: ắ Ắ`
- `Breve + Tilde: ẵ Ẵ`
- `Breve + Dot: m̐ M̐`
- `Breve + Dot Below: ặ Ặ`
- `Breve + Hook Above: ẳ Ẳ`
- `Comma: c̦ C̦ h̦ H̦ k̦ K̦ l̦ L̦ n̦ N̦ ș Ș ț Ț`
- `Comma Above: c̕ C̕ k̕ K̕ p̕ P̕ t̕ T̕`
- `Ogonek: ą Ą ę Ę į Į ų Ų`
- `Ogonek + Breve: c̨̆ C̨̆`
- `Hook Above: ả Ả ẻ Ẻ ỉ Ỉ ỏ Ỏ ủ Ủ ỷ Ỷ`
- `Dot: ḃ Ḃ Ċ ċ ė Ė Ḟ ḟ ġ Ġ ḣ Ḣ i İ ṁ Ṁ ṅ Ṅ ȯ Ȯ ṗ Ṗ ṙ Ṙ u̇ U̇ ẇ Ẇ Ẋ ẋ ẏ Ẏ ż Ż`
- `Dot Below: ạ Ạ c̣ C̣ ḍ Ḍ ẹ Ẹ ḥ Ḥ ị Ị ḳ Ḳ ḷ Ḷ ṃ Ṃ ṇ Ṇ ọ Ọ ṛ Ṛ ṣ Ṣ ṭ Ṭ ụ Ụ x̣ X̣ ỵ Ỵ ẓ Ẓ`
- `Dot + Macron: ȱ Ȱ`
- `Dot Below + Trema: ạ̈ Ạ̈ ọ̈ Ọ̈ ụ̈ Ụ̈`
- `Dot Below + Macron: ḹ Ḹ ṝ Ṝ ụ̄ Ụ̄`
- `Stroke: đ Đ ł Ł ƶ Ƶ`
- `Macron: ā Ā c̄ C̄ ē Ē ḡ Ḡ ī Ī k̄ K̄ n̄ N̄ ō Ō ū Ū ȳ Ȳ z̄ Z̄ ǣ Ǣ`
- `Macron Below: ḏ Ḏ ẖ H̱ ḵ Ḵ ḻ Ḻ ṉ Ṉ ṟ Ṟ ṯ Ṯ`
- `Macron + Acute: ī́ Ī́`
- `Horn: ơ Ơ ư Ư`
- `Horn + Grave: ờ Ờ ừ Ừ`
- `Horn + Acute: ớ Ớ ứ Ứ`
- `Horn + Tilde: ỡ Ỡ ữ Ữ`
- `Horn + Dot Below: ợ Ợ ự Ự`
- `Horn + Hook Above: ở Ở ử Ử`
- `Vowels: œ Œ æ Æ ø Ø ə Ə ı I`
- `Eszett: ß ẞ`
- `Eth: ð Ð`
- `Thorn: þ Þ`
- `Symbols: º ° ¡ ¿ § « » … © ® ™ ‡ • ʹ ʺ ʼ ˮ ¨ ʾ ʿ`
- `Currency: £ € ¥ ₹ ¢`

## Launch Parameters
You can start RVGL via command-line or shortcuts with the following parameters:

`-aspect <width> <height> <lens>`  
  Changes the display aspect ratio and fov (default 512). 

`-autokick`  
  Automatically kicks players who use altered cars (parameters.txt) or track files. 
  Cars and tracks are compared with the ones used by the host. (*Added in version `17.1009a`*.)

`-basepath <path>`  
  Set the base data path. If `<path>` is not provided, use the system default (if available) 
  or the current directory. (*Added in version `20.0902a`*.)

`-blocklist`  
  Enable the use of a global blocklist to block players from joining your session. The entries
  are loaded from `profiles\blocklist.txt` with one IP address per line. (*Added in version `21.0905a`*.)

`-chdir <path>`  
  Set the working directory to `<path>`. (*Added in version `19.0301a`*.)

`-cubevisi`  
  Performs simpler visibox computations for rendering.

`-gamegauge`  
  Launches game gauge benchmarking mode.

`-gogodemo`  
  Launches the game in demo mode.

`-hidechat`  
  Set to hide in-game chat messages. These can be seen later by pressing TAB or 
  F12. (*Added in version `19.0320a`*.)

`-largereplays`  
  Enable saving of large replays (up to 1GB), with a dynamically expanding replay 
  buffer size. (*Added in version `23.0501a`*.)

`-lobby <ip address>`  
  Launch the game and directly connect to a lobby. \<ip address\> can be the host's IP, 
  0 (to search for local sessions), or entirely omitted (for launching the host).

  As of version `23.0501a`, clients can specify the server port directly using a colon
  syntax (eg., 192.168.1.4:6666 or b.rv.gl:4444).

`-multisample <num_samples>`  
  Initializes a multisampled opengl window without framebuffer objects.

`-nodemo`  
  Disables screensaver activation at main menu. (*Added in version `19.0120a`*.)

`-noforce`  
  Disables force feedback of controllers.

`-nogamma`  
  Disables gamma correction.

`-nointro`  
  Disables the intro.

`-nojoy`  
  Disables the joystick and controller subsystem.

`-nolatejoin`  
  Set by the host to prevent new players from spectating an ongoing race. Helps 
  conserve bandwidth. (*Added in version `19.0414a`*.)

`-nomip`  
  Disables mipmaps.

`-nomulticast`  
  Set by the host to disable multicasting in P2P mode. Helps conserve bandwidth. 
  (*Added in version `19.0414a`*.)

`-nop2p`  
  Set by the host to disable P2P and use centralized connection mode.

`-nopause`  
  Keeps the game running even when it's in the background.

`-nores`  
  Disables display mode change in fullscreen. (*Added in version `20.0325a`*.)

`-noshader`  
  Force the legacy fixed pipeline renderer. (*Added in version `18.0428a`*.)

`-nosound`  
  Launches the game without sound and music.

`-nostars`  
  Disables stars in Museum 2's planetarium.

`-nouser`  
  Disables custom content.

`-nousercars`  
  Disables custom cars. (*Added in version `20.0210a`*.)

`-nousercups`  
  Disables custom cups. (*Added in version `20.1230a`*.)

`-nouserlevels`  
  Disables custom levels. (*Added in version `20.0210a`*.)

`-nousersfx`  
  Disables custom car sounds. (*Added in version `20.0210a`*.)

`-nouserskins`  
  Disables custom car skins. (*Added in version `20.0210a`*.)

`-packlist <name>`  
  Enable packs listed in the specified packlist. The game looks for the packlist 
  at `packs\<name>.txt`. (*Added in version `12.1230a`*.)

`-pass <phrase>`  
  Protects the session with a password. The passphrase can be up to 255 characters 
  in length and cannot contain spaces. (*Added in version `18.0310a`*.)

`-port <port>`  
  Overrides the LocalPort setting in rvgl.ini. (*Added in version `17.1009a`*.)

`-prefpath <path>`  
  Set the preferences path. Game saves and configuration files belong here. If `<path>` 
  is not provided, use the system default. This is usually:
  - `C:\Users\<user>\AppData\Roaming\RVGL` (Windows)  
  - `~/.local/share/RVGL` (Linux)

  (*Added in version `20.0902a`*.)

`-printlog`  
  Print the contents of the *re-volt.log* file to the terminal or console.

`-profile <name>`  
  Skip the profile selection menu and directly load the profile supplied. 
  If \<name\> is omitted, load the last used profile.

`-register <path>`  
  Register the game for lobby launching support. It's currently used for registering 
  a custom URI for the Discord client. (*Added in version `18.0731a`*.).

  As of version `23.0501a`, an optional path to the executable that must be registered 
  for lobby launching (Discord rich presence) can be passed after *-register*.

`-res <width> <height> <bpp>`  
  Forces a fullscreen resolution.

`-savereplays`  
  Automatically save replays in select game modes. (*Added in version `19.0210a`*.)

`-serverport <port>`  
  Overrides the port that the server listens to (default 2310). When the host uses 
  a custom server port, all clients will need to use the same server port to be 
  able to connect to that host. (*Added in version `18.0310a`*.).

  As of version `23.0501a`, clients can specify the server port directly using a colon
  syntax (eg., 192.168.1.4:6666 or b.rv.gl:4444).

`-sessionlog`  
  Saves race results and event logs (such as chat messages) to the `profiles` folder 
  as timestamped .csv and .log files. (*Added in version `17.1012a`*.)

`-showping`  
  Access ping display in Multi-Player mode with Ctrl + P. 
  (*Added in version `19.0210a`*.)

`-sload`  
  Disables the loading screen.

`-window <width> <height>`  
  Launches the game in window mode. Specify width and height optionally. 
  The default size is half the desktop dimensions.

**Dev**

`-alpharef <0 to 255>`  
  Reference value for alpha testing (default 128).

`-dev`  
  Launches the game in developer mode and enables edit modes and
  debug features.

`-editscale <s>`  
`-editoffset <x, y, z>`  
  Changes scale and offset of the in-game editor, 
  all floating point values

`-gazzasaicar`  
  Gazza's AI car. Have the player's car driven by AI and control AI cars 
  with Up / Down and Right Ctrl. To be used in conjunction with `-dev`.

`-gazzasaiinfo`  
  Gazza's AI info draw. Display debug info related to AI nodes and car 
  AI computations. To be used in conjunction with `-dev`.

`-gazzasroute`  
  Gazza's AI route choice. Additional debug options to force the route 
  decisions taken by AI cars. To be used in conjunction with `-dev`.

`-texinfo`  
  Shows the maximum texture size supported by the GPU.

