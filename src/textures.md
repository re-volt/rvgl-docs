# Textures

<!-- toc -->

> Applies to both cars and tracks.

## Texture Size

Supports standard texture sizes that work with any graphics card. Your texture 
has to be square (i.e., same width and height) with power-of-two dimensions 
(like 256x256, 512x512, etc.). The maximum supported texture size depends on 
your graphics card.

> The legacy Re-Volt versions only support 256x256 textures. To learn how to 
make your content backward compatible, see the Mipmaps section below.

## Mipmaps

Mipmaps are low resolution copies of the main texture. Mipmaps improve both the 
performance and visuals of your content, especially when used with hi-res textures.

Up to 10 mipmap levels can be used, each level must be half the size of the 
previous level and named with a different ".bm**p**" extension - the last character 
keeps increasing. (eg., a 256x256 "texture.bmp" will have a 128x128 "texture.bmq" file 
as the first mipmap level and a 64x64 "texture.bmr" as the second mipmap level).

We also support the use of mipmaps that go upward (i.e., with the above mentioned 
example, you may include a hi-res 512x512 version called "texture.bmo"). By 
including your hi-res texture this way, you will be able to keep the actual 
"texture.bmp" file at 256x256 and enable your content to be played in legacy 
Re-Volt versions.

For a quick reference, here is a list of supported sizes and their 
corresponding file names, as per the above example:
```
      8192 x 8192   -   texture.bmk
      4096 x 4096   -   texture.bml
      2048 x 2048   -   texture.bmm
      1024 x 1024   -   texture.bmn
       512 x 512    -   texture.bmo
     [ 256 x 256    -   texture.bmp ]
       128 x 128    -   texture.bmq
        64 x 64     -   texture.bmr
        32 x 32     -   texture.bms
        16 x 16     -   texture.bmt
         8 x 8      -   texture.bmu
         4 x 4      -   texture.bmv
         2 x 2      -   texture.bmw
         1 x 1      -   texture.bmx
```

> Intermediate mipmap levels cannot be skipped. For example, you cannot 
include a texture.bmp" (256x256) and a "texture.bmn" (1024x1024) without 
a "texture.bmo" (512x512) inbetween.

## Texture Sets

Mipmaps also find use as "texture sets". The game will pick textures of the 
appropriate size to load, depending on the available system resources. Therefore, 
it is always recommended to include appropriate mipmap levels for your content.

> Use the `-texinfo` command line to display the texture sets info that was detected 
for your system.


## Alpha Transparency

Textures with an alpha channel for transparency data is supported. The supported 
formats are 32-bit BMP (ARGB8888) and PNG.

Alpha channel transparency can be used to create rich and varied effects, eg., 
transparency gradients and well anti-aliased shapes. Colorkeying is disabled when 
using transparent textures, which means pure-black color (RGB 0,0,0) can be used 
on opaque surfaces.

>  Care should be taken to save your alpha-enabled texture in the ARGB format. 
Some other 32-bit BMP formats, such as XRGB, do not save the alpha channel in the 
extra space. This will result in a fully transparent texture in-game.

## Additional Texture Formats

RVGL supports several additional texture formats. For historical reasons, the 
textures should still be named with the .bmp extension (and mipmaps with .bmq, 
.bmo, etc. as usual).

Available formats: BMP, PNG, JPG, WEBP, TIF, GIF, SVG, CUR, ICO, LBM, PCX, PNM, XPM, XV, XCF. 

> Support for XCF (GIMP's native format) is experimental. This allows you to visualize a 
GIMP project file in RVGL without exporting it. However, this is not guaranteed to work 
correctly in all cases.

> Support for SVG was added in version `18.1020a`.  
