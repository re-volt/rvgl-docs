# RVGL Documentation

## About RVGL

RVGL is a cross-platform rewrite / port of Re-Volt that runs natively on both
Windows and GNU/Linux. It's powered entirely by modern, open source components.
Work in progress.

Send us your feedback at [The Re-Volt Hideout](https://forum.rvgl.org).

---

Copyright © RV Team 2010-2023.
Webpage: [https://rvgl.org](https://rvgl.org)
Email: [rv12@revoltzone.net](mailto:rv12@revoltzone.net)
